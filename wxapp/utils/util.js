var formatTime = function(t) {
    var e = t.getFullYear(), r = t.getMonth() + 1, m = t.getDate(), o = t.getHours(), a = t.getMinutes(), n = t.getSeconds();
    return [ e, r, m ].map(formatNumber).join("/") + " " + [ o, a, n ].map(formatNumber).join(":");
}, formatTime1 = function(t) {
    var e = t.getFullYear(), r = t.getMonth() + 1, m = t.getDate(), o = t.getHours(), a = t.getMinutes();
    t.getSeconds();
    return [ e, r, m ].map(formatNumber).join("-") + " " + [ o, a ].map(formatNumber).join(":");
}, formatNumber = function(t) {
    return (t = t.toString())[1] ? t : "0" + t;
};

module.exports = {
    formatTime: formatTime,
    formatTime1: formatTime1
};