var app = getApp(), util = require("../../utils/util.js");

Page({
    data: {
        date: [],
        s:"全部",
        o:"全部",
        r:"全部",
        page:1,
        swiper: {
            indicatorDots: !0,
            autoplay: !0,
            interval: 3e3,
            duration: 500,
            imgUrls: []
        },
        positionArr: [],
        positionArr1: [],
        positionArr2: [ {
            positions: "长期兼职",
            p_id: 1,
            types: "time"
        }, {
            positions: "短期兼职",
            p_id: 2,
            types: "time"
        }, {
            positions: "临时兼职",
            p_id: 3,
            types: "time"
        } ],
        jq: "",
        jq2: "",
        jq3: "",
        jqTy: "",
        ty: "",
        display: "none",
        display1: "none",
        display2: "none",
        display3: "none",
        a_id: "",
        footer: {
            footdex: 0,
            txtcolor: "#666666",
            seltxt: "#53cac3",
            background: "#fff",
            list: []
        },
        qu: "",
        a: !1,
        getUseInfo: !0,
        ps: !0
    },
    link_detail_ps: function(a) {
        wx.navigateTo({
            url: "/hyb_jianzhi/about_us/about_us?id=" + a.currentTarget.dataset.id
        });
    },
    getUsetInfo: function(a) {
        "getUserInfo:ok" == a.detail.errMsg ? (wx.setStorage({
            key: "useInfo",
            data: "true"
        }), this.close_modal(), this.getGetUid(a.detail.userInfo)) : this.setData({
            getUseInfo: !0
        });
    },
    close_modal: function(a) {
        this.setData({
            getUseInfo: !1
        });
    },
    swiperUrl: function(a) {
        var t = a.currentTarget.dataset.appid, e = (a.currentTarget.dataset.appsecret, a.currentTarget.dataset.lianjie);
        wx.navigateToMiniProgram({
            appId: t,
            path: e,
            extraData: {
                foo: "bar"
            }
        });
    },
    onLoad: function(a) {
        var t = this;
        app.util.request({
            url: "entry/wxapp/Base",
            success: function(a) {
                console.log(a.data.data), "1" == a.data.data.p_type ? (t.setData({
                    ps: !0
                }), t.getPiandaohang()) : (t.setData({
                    ps: !1
                }), t.getDaohang()), t.setData({
                    base: a.data.data,
                    a_id: a.data.data.cityname
                }), t.getDiqu();
                t.getJianzhi_info(a.data.data.cityname, "全部", "全部", "全部"), wx.setNavigationBarTitle({
                    title: a.data.data.name
                });
            }
        }), wx.getStorage({
            key: "useInfo",
            success: function(a) {
                "true" == a.data && t.setData({
                    getUseInfo: !1
                });
            }
        }), t.getPianlist(), t.getHuandengpian(), t.getJianzhi_style();
    },
    getGetUid: function(t) {
        wx.login({
            success: function(a) {
                a.code && app.util.request({
                    url: "entry/wxapp/GetUid",
                    data: {
                        code: a.code
                    },
                    success: function(a) {
                        a.data.errno || (wx.setStorageSync("openid", a.data.data.openid), console.log(a.data.data.openid), 
                        console.log(t), app.util.request({
                            url: "entry/wxapp/TyMember",
                            data: {
                                u_name: t.nickName,
                                u_thumb: t.avatarUrl,
                                u_sex: t.gender,
                                openid: a.data.data.openid
                            }
                        }));
                    }
                });
            }
        });
    },
    getHuandengpian: function() {
        var i = this;
        app.util.request({
            url: "entry/wxapp/Huandengpian",
            success: function(a) {
                console.log(a.data.data);
                for (var t = i.data.swiper, e = 0; e < a.data.data.length; e++) t.imgUrls = a.data.data;
                i.setData({
                    swiper: t
                });
            }
        });
    },
    getPianlist: function() {
        var t = this;
        app.util.request({
            url: "entry/wxapp/Pianlist",
            success: function(a) {
                t.setData({
                    pianlist: a.data.data
                });
            }
        });
    },
    getDiqu: function() {
        var i = this;
        app.util.request({
            url: "entry/wxapp/Diqu",
            success: function(a) {
                var t = a.data.data;
                for (var e in t) t[e].types = "position";
                i.setData({
                    positionArr: t
                });
            }
        });
    },
    getDaohang: function() {
        var e = this;
        app.util.request({
            url: "entry/wxapp/Daohang",
            success: function(a) {
                var t = e.data.footer;
                t.list = a.data.data, e.setData({
                    footer: t
                });
            }
        });
    },
    getPiandaohang: function() {
        var e = this;
        app.util.request({
            url: "entry/wxapp/Piandaohang",
            success: function(a) {
                var t = e.data.footer;
                console.log(a.data.data), t.list = a.data.data, e.setData({
                    footer: t
                });
            }
        });
    },
    tiaozhuan: function(a) {
        var t = a.currentTarget.dataset.appid, e = a.currentTarget.dataset.lianjie;
        wx.navigateToMiniProgram({
            appId: t,
            path: e,
            extraData: {
                foo: "bar"
            }
        });
    },
    getJianzhi_style: function() {
        var i = this;
        app.util.request({
            url: "entry/wxapp/Jianzhi_style",
            success: function(a) {
                var t = a.data.data;
                for (var e in t) t[e].types = "type";
                i.setData({
                    positionArr1: t
                });
            }
        });
    },
    getJianzhi_info: function(a, t, e, i) {
        var n = this;
        app.util.request({
            url: "entry/wxapp/Jianzhi_info",
            data: {
                city: a,
                quyu: t,
                leixing: e,
                shijian: i,
                page:n.data.page
            },
            success: function(a) {

              var arr = a.data.data;
              for (var i = 0, len = arr.length; i < len; i++) {
                arr[i].f_time = n.getDateDiff(arr[i].f_time);
              }

                n.setData({
                    jianzhi_info: a.data.data
                });
            }
        });
    },
    locationShowClick: function() {
        this.setData({
            display: "block",
            display1: "block",
            display2: "none",
            display3: "none",
            jqTy: "location"
        });
    },
    showClick: function() {
        this.setData({
            display: "none",
            display1: "none",
            display2: "none",
            display3: "none",
            jqTy: ""
        });
    },
    typeShowClick: function() {
        this.setData({
            display: "block",
            display1: "none",
            display2: "block",
            display3: "none",
            jqTy: "type"
        });
    },
    timeShowClick: function() {
        this.setData({
            display: "block",
            display1: "none",
            display2: "none",
            display3: "block",
            jqTy: "time"
        });
    },
    checkAllClick: function(a) {
        var t = this, e = a.currentTarget.dataset.types;
        if ("position" == e) var i = "", n = t.data.jq2, s = t.data.jq3; else if ("type" == e) i = t.data.jq, 
        n = "", s = t.data.jq3; else if ("time" == e) i = t.data.jq, n = t.data.jq2, s = "";
        t.setData({
            jq: i,
            jq2: n,
            jq3: s
        });
    },
    checkItemClick: function(a) {
        var t = this, e = a.currentTarget.dataset.types;
        if ("time" == e) var i = t.data.jq, n = t.data.jq2, s = a.currentTarget.dataset.j_id; else if ("type" == e) i = t.data.jq, 
        s = t.data.jq3, n = a.currentTarget.dataset.j_id; else if ("position" == e) i = a.currentTarget.dataset.j_id, 
        s = t.data.jq3, n = t.data.jq2;
        t.setData({
            jq: i,
            jq2: n,
            jq3: s,
            ty: e
        });
    },
    confirmClick: function(a) {
      
        var t = this, e = t.data.jq, i = t.data.jq2, n = t.data.jq3;
        t.setData({
          page:1
        });
        if ("" == e) var s = "全部"; else s = t.data.positionArr[e - 1].title;
        if ("" == i) var o = "全部"; else o = t.data.positionArr1[i - 1].title;
        if ("" == n) var r = "全部"; else r = t.data.positionArr2[n - 1].positions;
        var d = t.data.a_id;
        t.getJianzhi_info(d, s, o, r);
        t.setData({
            display: "none",
            display1: "none",
            display2: "none",
            display3: "none",
            jqTy: " "
        });
        t.setData({
          s:s,
          o:o,
          r:r
        })
    },
    choiceClick: function() {
        wx.navigateTo({
            url: "/hyb_jianzhi/position/position"
        });
    },
    searchClick: function() {
        wx.navigateTo({
            url: "/hyb_jianzhi/search/search?city=" + this.data.a_id
        });
    },
    detailClick: function(a) {
        var t = a.currentTarget.dataset.id;
        wx.navigateTo({
            url: "/hyb_jianzhi/jianzhixiangqing/index?f_id=" + t
        });
    },
    onShareAppMessage: function(a) {
        return "button" === a.from && console.log(a.target), {
            title: "自定义转发标题",
            path: "/page/user?id=123",
            success: function(a) {},
            fail: function(a) {}
        };
    },
    getDateDiff: function (dateTimeStamp){
      dateTimeStamp = Date.parse(dateTimeStamp);
      var minute = 1000 * 60;
      var hour = minute * 60;
      var day = hour * 24;
      var halfamonth = day * 15;
      var month = day * 30;
      var now = new Date().getTime();
      var diffValue = now - dateTimeStamp;
      if (diffValue < 0) { return; }
      var monthC = diffValue / month;
      var weekC = diffValue / (7 * day);
      var dayC = diffValue / day;
      var hourC = diffValue / hour;
      var minC = diffValue / minute;
      var result = "";
      if (monthC >= 1) {
        result = "" + parseInt(monthC) + "月前";
      }
      else if (weekC >= 1) {
        result = "" + parseInt(weekC) + "周前";
      }
      else if (dayC >= 1) {
        result = "" + parseInt(dayC) + "天前";
      }
      else if (hourC >= 1) {
        result = "" + parseInt(hourC) + "小时前";
      }
      else if (minC >= 1) {
        result = "" + parseInt(minC) + "分钟前";
      } else{
        result = "刚刚";
      }
        
      return result;
    },
  onPullDownRefresh: function () {
    this.setData({
      page:1
    })
    this.onLoad();
    wx.stopPullDownRefresh();
  },
  onShareAppMessage: function () {
    wx.showShareMenu({
      withShareTicket: true
    })
  },
  onReachBottom: function () {
    var pages=this.data.page+1;
    this.setData({
      page: pages
    });

    // console.log('到死了');

    var n = this;
    app.util.request({
      url: "entry/wxapp/Jianzhi_info",
      data: {
        city: n.data.a_id,
        quyu: n.data.s,
        leixing: n.data.o,
        shijian: n.data.r,
        page: n.data.page
      },
      success: function (a) {

        var arr = a.data.data;
        for (var i = 0, len = arr.length; i < len; i++) {
          arr[i].f_time = n.getDateDiff(arr[i].f_time);
        }


        var jianzhi_infos = n.data.jianzhi_info;
        for(var i=0;i<a.data.data.length;i++){
          jianzhi_infos.push(a.data.data[i]);
        }
        n.setData({
          jianzhi_info: jianzhi_infos
        });

      }
    });
  },


  
});