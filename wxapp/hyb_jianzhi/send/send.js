var app = getApp();

Page({
    data: {
        f_id: "",
        openid: "",
        userAge: "",
        index: 0,
        week: [{
          id: 1,
          name: '每周一',
        }, {
          id: 2,
          name: '每周二'
        }, {
          id: 3,
          name: '每周三'
        }, {
          id: 4,
          name: '每周四',
        },
        {
          id: 5,
          name: '每周五',
        },
        {
          id: 6,
          name: '每周六',
        },
        {
          id: 7,
          name: '每周天',
        }],
        current: ['每周六', '每周天'],
        position: 'right',
        checked: false,
        disabled: false,
        subdis:false
    },
    handleFruitChange({ detail = {} }) {
      const index = this.data.current.indexOf(detail.value);
      index === -1 ? this.data.current.push(detail.value) : this.data.current.splice(index, 1);
      this.setData({
        current: this.data.current
      });
    },
    onLoad: function(e) {
        var s = this;
        app.util.request({
            url: "entry/wxapp/User",
            data: {
                openid: wx.getStorageSync("openid")
            },
            success: function(e) {
                var a = e.data.data.u_chusheng, t = new Date().getFullYear() - a.split("-")[0];
                s.setData({
                    user: e.data.data,
                    userAge: t
                });
            }
        });
        var a = e.f_id;
        s.setData({
            f_id: a,
            openid: wx.getStorageSync("openid")
        });
    },
    inputClick: function(e) {
        var a = e.detail.cursor;
        this.setData({
            index: a
        });
    },
    subClick: function(e) {
      var that=this;
      wx.showLoading({
        title: '请稍后',
        mask: true
      })
        //console.log(e);
        var a = e.detail.value
        var cu = this.data.current;
        var freetime = '';
        for (var i in cu) {
          freetime = freetime.concat(cu[i],' ');
        }
        var a = e.detail.value;
        a.freetime = freetime;
      if ("" == a.youdian){
        wx.showToast({
          title: "请输入优点",
          image: "/hyb_jianzhi/images/errors.png"
        });
        return ;
      }
      that.setData({
        subdis:true
      });
      app.util.request({
          url: "entry/wxapp/Baoming",
          data: a,
          success: function(e) {
              wx.hideLoading();
              if(e.data.data == 1){
                that.setData({
                  subdis: true
                });
                wx.navigateTo({
                  url: "/hyb_jianzhi/success/success"
                });
              }else{
                that.setData({
                  subdis: false
                });
                wx.showToast({
                  title: "报名失败，请稍后重试",
                  success: function (e) {
                  }
                });
              }
          }
      });
    },
    onShareAppMessage: function() {},
  returnClick: function () {
    wx.reLaunch({
      url: "/hyb_jianzhi/home/home"
    });
  },
  jumpurl:function(){
    wx.navigateTo({
      url: "/hyb_jianzhi/user_diction/index"
    });
  }
});