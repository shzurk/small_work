var app = getApp();

Page({
    data: {
        nickName: "",
        avatarUrl: "",
        checked: "true",
        footer: {
            footdex: 2,
            txtcolor: "#666666",
            seltxt: "#53cac3",
            background: "#fff",
            list: [],
            current: 'homepage'
        }
    },
    handleChange({ detail }) {
      this.setData({
        current: detail.key
      });
    },
    onLoad: function(a) {
        var t = this;
        t.getDaohang(), t.getUser(), t.getShangjiainfo();
    },
    getDaohang: function() {
        var e = this;
        app.util.request({
            url: "entry/wxapp/Daohang",
            success: function(a) {
                var t = e.data.footer;
                t.list = a.data.data, e.setData({
                    footer: t
                });
            }
        });
    },
    getUser: function() {
        var t = this;
        app.util.request({
            url: "entry/wxapp/User",
            data: {
                openid: wx.getStorageSync("openid")
            },
            success: function(a) {
                t.setData({
                    user: a.data.data
                });
            },
            error:function(){
              
            }
        });
        console.log(wx.getStorageSync("openid"));
    },
    getShangjiainfo: function() {
        var t = this;
        app.util.request({
            url: "entry/wxapp/Shangjiainfo",
            data: {
                openid: wx.getStorageSync("openid")
            },
            success: function(a) {
                t.setData({
                    shangjiainfo: a.data.data
                });
            }
        });
    },
    editClick: function() {
        wx.navigateTo({
            url: "/hyb_jianzhi/edit/edit"
        });
    },
    applyClick: function() {
        wx.navigateTo({
            url: "/hyb_jianzhi/apply/apply"
        });
    },
    newstoreClick: function(a) {
        var t = a.currentTarget.dataset.typs;
        wx.navigateTo({
            url: "/hyb_jianzhi/newstore/newstore?typs=" + t
        });
    },
    myReleaseClick: function() {
        wx.navigateTo({
            url: "/hyb_jianzhi/my_release/my_release"
        });
    },
    myaddClick: function () {
      wx.navigateTo({
        url: "/hyb_jianzhi/fabu/fabu"
      });
    },
    classClick: function(a) {
        var t = a.currentTarget.dataset.checked;
        console.log(t), this.setData({
            checked: t
        });
    },
    payClick: function() {
        app.util.request({
            url: "entry/wxapp/Base",
            success: function(a) {
                0 < a.data.data.dp_money ? app.util.request({
                    url: "entry/wxapp/Pay",
                    data: {
                        openid: wx.getStorageSync("openid"),
                        zhiding_money: a.data.data.dp_money
                    },
                    header: {
                        "Content-Type": "application/json"
                    },
                    success: function(a) {
                        wx.requestPayment({
                            timeStamp: a.data.timeStamp,
                            nonceStr: a.data.nonceStr,
                            package: a.data.package,
                            signType: a.data.signType,
                            paySign: a.data.paySign,
                            success: function(a) {
                                app.util.request({
                                    url: "entry/wxapp/Rusave",
                                    data: {
                                        openid: wx.getStorageSync("openid"),
                                        zhiding_money: a.data.data.dp_money
                                    },
                                    success: function(a) {
                                        wx.showToast({
                                            title: "置顶成功"
                                        });
                                    }
                                });
                            }
                        });
                    }
                }) : app.util.request({
                    url: "entry/wxapp/Rusave",
                    data: {
                        openid: wx.getStorageSync("openid"),
                        zhiding_money: a.data.data.dp_money
                    },
                    success: function(a) {
                        wx.showToast({
                            title: "置顶成功"
                        });
                    }
                });
            }
        });
    },
    jumpurl: function (e) {
      var id = e.currentTarget.id;
      // var app = getApp();
      // app.requestDetailid=id;
      console.log(id);
      wx.navigateTo({
        url: id
      })
    },
    onShareAppMessage: function() {}
});