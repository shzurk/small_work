var app = getApp();

Page({
    data: {
        city: "",
        conB: "",
        content: "",
        heightWork:'',
        conArr: [ {
            de_id: 0,
            bgColor: "#f60",
            brief: "派单",
            title: "长期招聘派单拉访兼职",
            hours: "03月14日起工作（共18天）",
            clock: "2018-03-27",
            pay: "60元/天",
            position: "迎泽"
        }, {
            de_id: 1,
            bgColor: "#f60",
            brief: "派单",
            title: "长期招聘派单拉访兼职",
            hours: "03月14日起工作（共18天）",
            clock: "2018-03-26",
            pay: "60元/天",
            position: "迎泽"
        }, {
            de_id: 2,
            bgColor: "#f60",
            brief: "派单",
            title: "长期招聘派单拉访兼职",
            hours: "03月14日起工作（共18天）",
            clock: "2018-03-20",
            pay: "60元/天",
            position: "迎泽"
        }, {
            de_id: 3,
            bgColor: "#f60",
            brief: "派单",
            title: "长期招聘派单拉访兼职",
            hours: "03月14日起工作（共18天）",
            clock: "2018-03-21",
            pay: "60元/天",
            position: "迎泽"
        }, {
            de_id: 4,
            bgColor: "#f60",
            brief: "派单",
            title: "长期招聘派单拉访兼职",
            hours: "03月14日起工作（共18天）",
            clock: "2018-02-25",
            pay: "60元/天",
            position: "迎泽"
        }, {
            de_id: 5,
            bgColor: "#f60",
            brief: "派单",
            title: "长期招聘派单拉访兼职",
            hours: "03月14日起工作（共18天）",
            clock: "2017-03-19",
            pay: "60元/天",
            position: "迎泽"
        } ]
    },
    onLoad: function(t) {
        var i = t.city;
      var gxjianzhi = t.heightWork;
        this.setData({
            city: i,
          heightWork: gxjianzhi
        }), this.getJianzhi_style();
      if (this.data.heightWork!='')
        this.gaoxjianzhi();

    },
    getJianzhi_style: function() {
        var i = this;
        app.util.request({
            url: "entry/wxapp/Jianzhi_style",
            success: function(t) {
                i.setData({
                    positionArr1: t.data.data
                });
            }
        });
    },
    searchBlur: function(t) {
        if ("" == t.detail.value) {
            var i = t.detail.value;
            this.setData({
                conB: i
            });
        }
    },
    subClick: function(t) {
        var i = this, a = t.detail.value, e = t.detail.value.content;
        console.log(e), a.city = i.data.city, app.util.request({
            url: "entry/wxapp/Jianzhi_lists",
            data: a,
            success: function(t) {
                var arr = t.data.data;
                for (var ii = 0, len = arr.length; ii < len; ii++) {
                  arr[ii].f_time = i.getDateDiff(arr[ii].f_time);
                }
                i.setData({
                    list: t.data.data,
                    conB: e
                });
            }
        });
    },
    hotClick: function(t) {
        var i = this, a = t.currentTarget.dataset.h_con, e = t.currentTarget.dataset.h_con;
        console.log(a, e);
        var o = i.data.city;
        app.util.request({
            url: "entry/wxapp/Jianzhi_list",
            data: {
                con: a,
                city: o
            },
            success: function(t) {
              var arr = t.data.data;
              for (var ii = 0, len = arr.length; ii < len; ii++) {
                arr[ii].f_time = i.getDateDiff(arr[ii].f_time);
              }
                console.log(t.data.data), i.setData({
                    list: t.data.data,
                    conB: e
                });
            }
        });
    },
    gaoxjianzhi:function(e){
      var i = this;
      var a = this.data.heightWork;
      var e = this.data.heightWork;
      var o = i.data.city;
      app.util.request({
        url: "entry/wxapp/Jianzhi_list",
        data: {
          con: a,
          city: o
        },
        success: function (t) {
          var arr = t.data.data;
          for (var ii = 0, len = arr.length; ii < len; ii++) {
            arr[ii].f_time = i.getDateDiff(arr[ii].f_time);
          }
          console.log(t.data.data), i.setData({
            list: t.data.data,
            conB: e
          });
        }
      });
    },
    detailClick: function(t) {
        var i = t.currentTarget.dataset.id;
        wx.navigateTo({
          url: "/hyb_jianzhi/jianzhixiangqing/index?f_id=" + i
        });
    },
    returnClick: function() {
        wx.reLaunch({
            url: "/hyb_jianzhi/home/home"
        });
    },
    onShareAppMessage: function() {},
  getDateDiff: function (dateTimeStamp) {
    dateTimeStamp = Date.parse(dateTimeStamp);
    var minute = 1000 * 60;
    var hour = minute * 60;
    var day = hour * 24;
    var halfamonth = day * 15;
    var month = day * 30;
    var now = new Date().getTime();
    var diffValue = now - dateTimeStamp;
    if (diffValue < 0) { return; }
    var monthC = diffValue / month;
    var weekC = diffValue / (7 * day);
    var dayC = diffValue / day;
    var hourC = diffValue / hour;
    var minC = diffValue / minute;
    var result = "";
    if (monthC >= 1) {
      result = "" + parseInt(monthC) + "月前";
    }
    else if (weekC >= 1) {
      result = "" + parseInt(weekC) + "周前";
    }
    else if (dayC >= 1) {
      result = "" + parseInt(dayC) + "天前";
    }
    else if (hourC >= 1) {
      result = "" + parseInt(hourC) + "小时前";
    }
    else if (minC >= 1) {
      result = "" + parseInt(minC) + "分钟前";
    } else {
      result = "刚刚";
    }

    return result;
  }
});