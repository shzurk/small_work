//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
   
  },
  //事件处理函数
 
  onLoad: function () {
    var t = this;
    app.util.request({
      url: "entry/wxapp/GetPayRecord",
      data: {
        openid: wx.getStorageSync("openid")
      },
      success: function (a) {
        t.setData({
          list: a.data.data
        });
      },
      error: function () {

      }
    });
  },
  
})
