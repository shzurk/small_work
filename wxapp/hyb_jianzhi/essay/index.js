var app = getApp(), util = require("../../utils/util.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    html:'<p>从明天起，做一个幸福的人</p><p>喂马，砍柴，周游世界</p><p>从明天起，关心粮食和蔬菜</p><p>我有一所房子，面朝大海，春暖花开</p><p><br/></p><p>从明天起做一个幸福的人</p><p>喂马，砍柴，周游世界</p><p>从明天起，关心粮食和蔬菜</p><p>我有一所房子，面朝大海，春暖花开</p><p><br/></p><p>从明天起做一个幸福的人</p><p>喂马，砍柴，周游世界</p><p>从明天起，关心粮食和蔬菜</p><p>我有一所房子，面朝大海，春暖花开</p><p><br/></p>',

    content:{}
  },

  strcharacterDiscode:function (str) {
    // 加入常用解析    
    str = str.replace(/&nbsp;/g, ' ');
    str = str.replace(/&quot;/g, "'");
    str = str.replace(/&amp;/g, '&');
    // str = str.replace(/&lt;/g, '‹');    
    // str = str.replace(/&gt;/g, '›');    
    str = str.replace(/&lt;/g, '<');
    str = str.replace(/&gt;/g, '>');
    str = str.replace(/&#8226;/g, '•');
    return str;
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    /**精品兼职 */
    var t = this;
    app.util.request({
      url: "entry/wxapp/Goodonelist",
      data: {
        id: options.id,
      },
      success: function (a) {
        // a.data.data.content = t.strcharacterDiscode(a.data.data[0].content)
        t.setData({
          content: a.data.data[0]
        })
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
  detailClick: function (a) {
    var t = a.currentTarget.dataset.id;
    wx.navigateTo({
      url: "/hyb_jianzhi/jianzhixiangqing/index?f_id=" + t
    });
  },
  

  
})