var app = getApp();

Page({
    data: {
        caozuo: "",
        uplogo: "",
        uplogos: "",
        choosep: !1,
        name: "",
        email: "",
        c_name: "",
        indexjobNature: 0,
        arrayjobNature: [ "10人以下", "10-20人", "20人以上" ],
        fenlei: [ {
            name: "互联网/IT/电子/通信",
            zifenlei: [ "电子商务", "游戏", "媒体", "广告营销", "数据服务", "医疗健康", "生活服务", "o2o", "旅游", "分类信息", "音乐/视频/阅读", "在线教育", "社交网络", "人力资源服务", "企业服务", "信息安全", "新零售", "智能硬件", "移动互联网", "互联网", "计算机软件", "计算机硬件", "计算机服务", "通信/网络设备", "运营商/增值服务", "电子/半导体/集成电路" ]
        }, {
            name: "广告/传媒/文化/体育",
            zifenlei: [ "广告/公关/会展", "新闻/出版", "广播/影视", "文化/体育/娱乐" ]
        }, {
            name: "金融",
            zifenlei: [ "银行", "保险", "证券/期货", "基金", "信托", "互联网金融", "投资/融资", "租赁/拍卖/典当/担保" ]
        }, {
            name: "教育培训",
            zifenlei: [ "学前教育", "院校", "培训机构", "学术科研" ]
        }, {
            name: "制药/医疗",
            zifenlei: [ "制药", "医疗/护理/卫生", "医疗/设备/器械" ]
        }, {
            name: "交通/物流/贸易/零售",
            zifenlei: [ "交通/运输", "物流/仓储", "批发/零售", "贸易/进出口" ]
        }, {
            name: "专业服务",
            zifenlei: [ "咨询", "法律", "翻译", "人力资源服务", "财务/审计/税务", "检测/认证", "专利/商标/知识产权", "其他专业服务" ]
        }, {
            name: "房地产/建筑",
            zifenlei: [ "房地产开发", "工程施工", "建筑设计", "装修装饰", "建材", "地产经纪/中介", "物业服务" ]
        }, {
            name: "汽车",
            zifenlei: [ "汽车生产", "汽车零部件", "4S店/后市场" ]
        }, {
            name: "机械/制造",
            zifenlei: [ "机械设备/机电/重工", "仪器仪表/工业自动化", "原材料及加工/模具", "印刷/包装/造纸", "船舶/航空/航天" ]
        }, {
            name: "消费品",
            zifenlei: [ "食品/饮料/烟酒", "日化", "服装/纺织/皮革", "家具/家电/家居", "玩具/礼品", "珠宝/首饰", "工艺品/收藏品", "办公用品及设备" ]
        }, {
            name: "服务业",
            zifenlei: [ "餐饮", "酒店", "旅游", "美容/美发", "婚庆/摄影", "其他服务业" ]
        }, {
            name: "能源/化工/环保",
            zifenlei: [ "石油/石化", "化工", "矿产/地质", "采掘/冶炼", "电力/热力/燃气/水利", "新能源", "环保" ]
        }, {
            name: "政府/非盈利机构/其他",
            zifenlei: [ "政府/公共事业", "非盈利机构", "农/林/牧/渔", "其他行业" ]
        } ]
    },
    save_name: function(e) {
        this.setData({
            c_name: e.detail.value
        });
    },
    save_email: function(e) {
        this.setData({
            email: e.detail.value
        });
    },
    uploadImg: function() {
        var t = this, o = t.data.uniacid;
        wx.chooseImage({
            count: 1,
            sizeType: [ "original", "compressed" ],
            sourceType: [ "album", "camera" ],
            success: function(e) {
                var a = e.tempFilePaths[0];
                t.setData({
                    upvimage: a
                }), wx.uploadFile({
                    url: t.data.url + "app/index.php?i=" + o + "&c=entry&a=wxapp&do=upload&m=hyb_home",
                    filePath: a,
                    name: "upfile",
                    formData: {},
                    success: function(e) {
                        console.log(e.data), t.setData({
                            uplogo: e.data
                        });
                    }
                });
            }
        });
    },
    uploadImg2: function() {
        var t = this, o = t.data.uniacid;
        wx.chooseImage({
            count: 1,
            sizeType: [ "original", "compressed" ],
            sourceType: [ "album", "camera" ],
            success: function(e) {
                var a = e.tempFilePaths[0];
                t.setData({
                    upvimage: a
                }), wx.uploadFile({
                    url: t.data.url + "app/index.php?i=" + o + "&c=entry&a=wxapp&do=upload&m=hyb_home",
                    filePath: a,
                    name: "upfile",
                    formData: {},
                    success: function(e) {
                        console.log(e.data), t.setData({
                            uplogos: e.data
                        });
                    }
                });
            }
        });
    },
    formSubmit: function(e) {
        var a = e.detail.value;
        "" == a.c_name ? wx.showToast({
            title: "请填写公司名称",
            image: "/hyb_home/resource/images/error.png"
        }) : "" == a.hangye ? wx.showToast({
            title: "请选择所属行业",
            image: "/hyb_home/resource/images/error.png"
        }) : "" == a.email ? wx.showToast({
            title: "请填写公司邮箱",
            image: "/hyb_home/resource/images/error.png"
        }) : /^[a-zA-Z0-9_-]+@([a-zA-Z0-9]+\.)+(com|cn|net|org)$/.test(a.email) ? "" == a.yingyezhizhao ? wx.showToast({
            title: "请上传营业执照",
            image: "/hyb_home/resource/images/error.png"
        }) : "" == a.mendian ? wx.showToast({
            title: "请上传门店照片",
            image: "/hyb_home/resource/images/error.png"
        }) : (console.log(a), app.util.request({
            url: "entry/wxapp/Gongsi",
            data: a,
            success: function(e) {
                wx.showToast({
                    title: "提交成功"
                }), setTimeout(function() {
                    wx.navigateTo({
                        url: "/hyb_home/index/index"
                    });
                }, 1e3);
            }
        })) : (wx.showToast({
            title: "邮箱格式不正确",
            image: "/hyb_home/resource/images/error.png"
        }), this.setData({
            email: ""
        }));
    },
    bindjobNatureChange: function(e) {
        this.setData({
            indexjobNature: e.detail.value
        });
    },
    open_choose: function(e) {
        this.setData({
            choosep: !0
        });
    },
    choosePosition: function(e) {
        var a = e.currentTarget.dataset.name;
        console.log(a), this.setData({
            choosep: !1,
            name: a
        });
    },
    onLoad: function(e) {
        var a = this, t = app.siteInfo.uniacid, o = wx.getStorageSync("openid"), i = e.caozuo;
        a.setData({
            openid: o,
            uniacid: t,
            caozuo: i
        }), app.util.request({
            url: "entry/wxapp/url",
            success: function(e) {
                a.setData({
                    url: e.data
                });
            }
        }), a.getUsergongsi(o);
    },
    getUsergongsi: function(e) {
        var i = this;
        app.util.request({
            url: "entry/wxapp/Usergongsi",
            data: {
                openid: e
            },
            success: function(e) {
                for (var a = e.data.data.g_guimo, t = i.data.arrayjobNature, o = 0; o < t.length; o++) a == t[o] && i.setData({
                    indexjobNature: o
                });
                i.setData({
                    gongsi: e.data.data,
                    uplogo: e.data.data.g_mendian,
                    uplogos: e.data.data.g_yingyezhizhao
                });
            }
        });
    },
    onShareAppMessage: function() {}
});