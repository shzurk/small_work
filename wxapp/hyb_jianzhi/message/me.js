var app = getApp();
Page({
  data: {

  },
  getcontent: function (event) {
    var message_id = event.currentTarget.dataset.messageId
    wx.navigateTo({
      url: '/hyb_jianzhi/messagecontent/index?message_id=' + message_id,
    })
  },
  onLoad: function (e) {
    var that = this;
    app.util.request({
      url: "entry/wxapp/GetMessage",
      data: {
        is_user:1,
        openid: wx.getStorageSync("openid")
      },
      success: function (a) {
        that.setData({
          list: a.data.data
        })
        console.log(a)
      },
      error: function () {

      }
    });
  },
  onPullDownRefresh: function () {
    this.onLoad();
    wx.stopPullDownRefresh();
  }

})