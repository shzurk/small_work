Page({
    data: {
        time: "获取验证码",
        disabled: !1,
        phone: ""
    },
    onLoad: function(t) {
        if (t.phone) {
            var e = t.phone;
            this.setData({
                phone: e
            });
        }
    },
    photoReg: function(t) {
        var e = new RegExp("^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$", "g"), n = t.detail.value;
        e.test(n) || wx.showModal({
            title: "提示",
            content: "请输入正确的电话"
        });
    },
    timeClick: function() {
        var t = this, e = 10, n = null;
        t.setData({
            disabled: !0
        }), n = setInterval(function() {
            t.setData({
                time: e + "秒后重新发送"
            }), 0 == --e && clearInterval(n);
        }, 1e3), setTimeout(function() {
            t.setData({
                time: "重新获取验证码",
                disabled: !1
            });
        }, 12e3);
    },
    getPhoto: function(t) {
        var e = t.detail.value.photo;
        wx.redirectTo({
            url: "/hyb_jianzhi/edit/edit?photo=" + e
        });
    },
    onReady: function() {},
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    onPullDownRefresh: function() {},
    onReachBottom: function() {},
    onShareAppMessage: function() {}
});