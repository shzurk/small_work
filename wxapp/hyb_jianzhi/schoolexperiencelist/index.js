var app = getApp();
const { $Message } = require('../../dist/base/index');
Page({
  data: {
    deleteId:0,
    visible:false,
    actions: [
      {
        name: '取消'
      },
      {
        name: '删除',
        color: '#ed3f14',
        loading: false
      }
    ]
  },

  onLoad: function (e) {
    var that=this;
    app.util.request({
      url: "entry/wxapp/GetExperience",
      data: {
        openid: wx.getStorageSync("openid")
      },
      success: function (a) {
        that.setData({
          list: a.data.data
        })
      },
      error: function () {

      }
    });
  },
  addExperience: function(){
    wx.navigateTo({
      url: '/hyb_jianzhi/schoolexperience/index',
    })
  },
  update:function(event){
    var experience_id=event.currentTarget.dataset.experienceId;
    wx.navigateTo({
      url: '/hyb_jianzhi/schoolexperienceupdate/index?experience_id=' + experience_id,
    })
  },
  deleteEx:function(event){
    var experience_id = event.currentTarget.dataset.experienceId;
    this.setData({
      deleteId:experience_id,
      visible:true
    })
  
  },

  deleteById({ detail }) {
    if (detail.index === 0) {
      this.setData({
        visible: false
      });
    } else {
      this.setData({
        actions: [
          { name: '取消'},
          {name: '删除',
            color: '#ed3f14',
            loading: true}]
      });

    var that=this;
      app.util.request({
        url: "entry/wxapp/DeleteExperience",
        data: {
          experience_id: that.data.deleteId,
          openid: wx.getStorageSync("openid")
        },
        success: function (data) {
          if (data.data.message == "success"){
            that.setData({
              visible: false,
              actions: [
                { name: '取消' },
                {
                  name: '删除',
                  color: '#ed3f14',
                  loading: false
                }],
              list: data.data.data
            });
   
          }
          else{
            $Message({
              content: '删除失败！',
              type: 'error'
            });
          }
  
        },
        error: function () {

        }
      });
    }
  },
  returnClick: function () {
    wx.reLaunch({
      url: "/hyb_jianzhi/home/home"
    });
  },
})