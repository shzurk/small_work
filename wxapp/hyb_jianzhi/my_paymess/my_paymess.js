var app = getApp();

Page({
    data: {
        mess: [ {
            id: 0,
            date: "12月15日",
            time: "15:20",
            who_pay: "积分支付",
            buckle: 5,
            content: "置顶或付费消息"
        }, {
            id: 1,
            date: "12月15日",
            time: "15:20",
            who_pay: "积分支付",
            buckle: 5,
            content: "置顶或付费消息"
        } ]
    },
    link_url: function(t) {
        var a = t.currentTarget.dataset.index;
        wx.navigateTo({
            url: "../xx_detail/xx_detail?id=" + a
        });
    },
    onLoad: function(t) {
        var a = wx.getStorageSync("openid");
        this.getZhiding(a);
    },
    getZhiding: function(t) {
        var a = this;
        app.util.request({
            url: "entry/wxapp/Zhidinglist",
            data: {
                openid: t
            },
            success: function(t) {
                a.setData({
                    list: t.data.data
                });
            },
            fail: function(t) {
                console.log(t);
            }
        });
    },
    onShareAppMessage: function() {}
});