// pages/usercenter/index.js

const { $Toast } = require('../../dist/base/index');
const { $Message } = require('../../dist/base/index');
var app = getApp();
Page({

  data: {
    experience_addr: "",
    experience_prize: "",
    start_time: '请选择时间',
    end_time: '请选择时间',
    experience_content: ""
  },

  bindaddr: function (e) {
    this.setData({
      experience_addr: e.detail.detail.value
    })
  },

  bindprize: function (e) {
    this.setData({
      experience_prize: e.detail.detail.value
    })
  },

  bindcontent: function (e) {
    this.setData({
      experience_content: e.detail.detail.value
    })
  },

  bindStartDateChange: function (e) {
    this.setData({
      start_time: e.detail.value
    })
  },

  bindEndDateChange: function (e) {
    this.setData({
      end_time: e.detail.value
    })
  },

  submit: function () {
    if (this.data.experience_addr == "") {
      $Toast({
        content: '组织名称不能为空',
        type: 'warning'
      });
      return;
    }
    if (this.data.experience_prize == "") {
      $Toast({
        content: '职位或奖励不能为空',
        type: 'warning'
      });
      return;
    }
    if (this.data.start_time == '请选择时间') {
      $Toast({
        content: '开始时间不能为空',
        type: 'warning'
      });
      return;
    }
    if (this.data.end_time == '请选择时间') {
      $Toast({
        content: '结束时间不能为空',
        type: 'warning'
      });
      return;
    }
    if (this.data.experience_content == "") {
      $Toast({
        content: '经历描述不能为空',
        type: 'warning'
      });
      return;
    }
    var that = this;
    $Toast({
      content: '正在操作',
      icon: 'prompt',
      duration: 0,
      mask: false
    });
    app.util.request({
      url: "entry/wxapp/InsertExperience",
      data: {
        experience_addr: that.data.experience_addr,
        experience_prize: that.data.experience_prize,
        start_time: that.data.start_time,
        end_time: that.data.end_time,
        experience_content: that.data.experience_content,
        openid: wx.getStorageSync("openid")
      },
      success: function (data) {
        $Toast.hide();
        console.log(data.data);
        if (data.data.message == "success") {
          $Message({
            content: '添加成功',
            type: 'success'
          });
          setTimeout(() => {
            wx.navigateTo({
              url: '/hyb_jianzhi/schoolexperiencelist/index',
            });
          }, 2000);
        }
        else
          $Message({
            content: '添加失败',
            type: 'error'
          });
      },
      error: function () {

      }
    });
  },
  returnClick: function () {
    wx.reLaunch({
      url: "/hyb_jianzhi/home/home"
    });
  },
})