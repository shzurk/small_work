var app = getApp();

Page({
    data: {
        qu: "",
        my: ""
    },
    onLoad: function(a) {
        var t = a.f_id, e = a.my;
        this.setData({
            my: e
        }), this.getJianzhixq(t);
    },
    getJianzhixq: function(a) {
        var i = this;
        app.util.request({
            url: "entry/wxapp/Jianzhi_infoxq",
            data: {
                f_id: a
            },
            success: function(a) {
                var t = a.data.data.f_address, e = new RegExp(/[市|区|县]/);
                t = t.split(e), i.setData({
                    jianzhi_infoxq: a.data.data,
                    qu: t[1]
                });
            }
        });
    },
    enrollClick: function(t) {
        console.log(wx.getStorageSync("openid")), console.log(t.currentTarget.dataset.f_openid), 
        app.util.request({
            url: "entry/wxapp/User",
            data: {
                openid: wx.getStorageSync("openid")
            },
            success: function(a) {
                null == a.data.data.u_bname ? wx.showModal({
                    title: "提示",
                    content: "请先完善信息",
                    success: function(a) {
                        a.confirm && wx.navigateTo({
                            url: "/hyb_jianzhi/edit/edit"
                        });
                    }
                }) : "待审核" == a.data.data.u_typs ? wx.showToast({
                    title: "请等待审核",
                    image: "/hyb_jianzhi/images/errors.png"
                }) : wx.getStorageSync("openid") == t.currentTarget.dataset.f_openid ? wx.showToast({
                    title: "您是发布者",
                    image: "/hyb_jianzhi/images/errors.png"
                }) : "不限" != t.currentTarget.dataset.sex && a.data.data.u_sex != t.currentTarget.dataset.sex ? wx.showToast({
                    title: "不符合需求",
                    image: "/hyb_jianzhi/images/errors.png"
                }) : app.util.request({
                    url: "entry/wxapp/Userbaiming",
                    data: {
                        openid: wx.getStorageSync("openid"),
                        f_id: t.currentTarget.dataset.f_id
                    },
                    success: function(a) {
                        0 == a.data.data ? wx.reLaunch({
                            url: "/hyb_jianzhi/send/send?f_id=" + t.currentTarget.dataset.f_id
                        }) : wx.showToast({
                            title: "您已报名",
                            image: "/hyb_jianzhi/images/errors.png"
                        });
                    }
                });
            }
        });
    },
    returnClick: function() {
        wx.redirectTo({
            url: "/hyb_jianzhi/index/index"
        });
    },
    mapClick: function(a) {
        wx.openLocation({
            latitude: parseFloat(a.currentTarget.dataset.latitude),
            longitude: parseFloat(a.currentTarget.dataset.longitude),
            address: a.currentTarget.dataset.address,
            scale: 22
        });
    },
    jumpurl: function (e) {
      var id = e.currentTarget.id;
      // var app = getApp();
      // app.requestDetailid=id;
      console.log(id);
      wx.navigateTo({
        url: id
      })
    },
    onShareAppMessage: function() {},
  returnClick: function () {
    wx.reLaunch({
      url: "/hyb_jianzhi/home/home"
    });
  },
});