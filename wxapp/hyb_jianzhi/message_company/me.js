var app = getApp();
Page({
  data: {
    current: "remind"
  },
  getcontent: function (event) {
    var message_id = event.currentTarget.dataset.messageId
    wx.navigateTo({
      url: '/hyb_jianzhi/messagecontent_company/index?message_id=' + message_id,
    })
  },
  onLoad: function (e) {
    var that = this;
    app.util.request({
      url: "entry/wxapp/GetMessage",
      data: {
        is_user:0,
        openid: wx.getStorageSync("openid")
      },
      success: function (a) {
        that.setData({
          list: a.data.data
        })
        console.log(a)
      },
      error: function () {

      }
    });

    app.util.request({
      url: "entry/wxapp/GetNolookMsg",
      data: {
        openid: wx.getStorageSync("openid")
      },
      success: function (a) {
        that.setData({
          no_msg: a.data.data
        });

      },
      error: function () {
      }
    });
  },
  returnClick: function () {
    wx.reLaunch({
      url: "/hyb_jianzhi/home/home"
    });
  },
  jumpurl: function (e) {
    var id = e.currentTarget.id;
    // var app = getApp();
    // app.requestDetailid=id;
    console.log(id);
    wx.reLaunch({
      url: id
    })
  },
  onPullDownRefresh: function () {
    this.onLoad();
    wx.stopPullDownRefresh();
  },
  handleChange({ detail }) {
    //console.log(detail);
    this.setData({
      current: detail.key
    });
    if (detail.key == 'homepage') {
      wx.reLaunch({
        url: '../my_release0/my_release'
      })
    }
    if (detail.key == 'group') {
      wx.reLaunch({
        url: '../fabu/fabu'
      })
    }
    if (detail.key == 'remind') {
      wx.reLaunch({
        url: '../message_company/me'
      })
    }
    if (detail.key == 'mine') {
      wx.reLaunch({
        url: '../newstore/newstore'
      })
    }
  }


})