const { $Message } = require('../../dist/base/index');
const { $Toast } = require('../../dist/base/index');
var app = getApp();
Page({
  data: {
    starIndex: 0,
    fir: '工作表现良好，非常认真努力',
    fir2: '非常勤奋，很快适用工作，积极性很高',
    fir3: '这样的兼职生给我来一打~~工作非常棒',
    precomment: ''
  },
  onChange(e) {
    this.setData({
      'starIndex': e.detail.index
    })
  },
  bindprecomment: function (e) {
    this.setData({
      'precomment': e.detail.index
    })
  },
  onLoad:function(e){
    this.setData({
      b_id: e.b_id,
      f_id: e.f_id,
      f_name: e.f_name,
      user_openid: e.user_openid
    })
  },
  clickMe: function(e){
       this.setData({
           'precomment':this.data.fir,
       })
    },
    clickMe2: function (e) {
        this.setData({
            'precomment': this.data.fir2,
        })
    },
    clickMe3: function (e) {
        this.setData({
            'precomment': this.data.fir3,
        })
    },
  submitComment:function(e){
    var that=this;
    app.util.request({
      url: "entry/wxapp/CompanyComment",
      data: {
        b_id: that.data.b_id,
        f_id: that.data.f_id,
        company_openid: wx.getStorageSync("openid"),
        user_openid: that.data.user_openid,
        comment_type: that.data.starIndex,
        comment_message: that.data.precomment
      },
      success: function (data) {
        if (data.data.message == "success") {
          $Message({
            content: '操作成功',
            type: 'success'
          });
          that.Deletecache();
          setTimeout(() => {
            wx.navigateTo({
              url: "/hyb_jianzhi/apply1/apply1?f_id=" + that.data.f_id + "&f_name=" + that.data.f_name,
            });
          }, 2000);
        }
        else
          $Message({
            content: '操作失败',
            type: 'error'
          });
      }
    });
    
  },
  returnClick: function () {
    wx.reLaunch({
      url: "/hyb_jianzhi/home/home"
    });
  },

  Deletecache: function () {
    var a = {};
    a.f_id = this.data.f_id;
    a.f_name = this.data.f_name;
    var pages = getCurrentPages();
    if (pages.length > 1) {
      var prePage = pages[pages.length - 2];
      prePage.onLoad(a);
    }
    if (pages.length > 2) {
      var preprePage = pages[pages.length - 3];
      preprePage.onLoad()
    }
  }



});