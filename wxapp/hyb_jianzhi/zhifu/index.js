Page({
  data: {
    current: 'tab1',
    current_scroll: 'tab1',
    bg:'#aaaaaa',
    bg2:'#aaaaaa'
  },

  handleChange({ detail }) {
    this.setData({
      current: detail.key
    });
  },

  handleChangeScroll({ detail }) {
    this.setData({
      current_scroll: detail.key
    });
  },
  change1(){
 
    this.setData({ bg: '#3FAEE5' })
    this.setData({ bg2: '#aaaaaa' })

  },
    change2() {
      this.setData({ bg: '#aaaaaa' })
      this.setData({ bg2: '#3FAEE5' })
  }
});