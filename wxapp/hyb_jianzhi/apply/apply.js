var app = getApp();

Page({
    data: {
        navArr: [ {
            title: "全部",
            n_id: 0
        }, {
            title: "已报名",
            n_id: 1
        }, {
            title: "已录用",
            n_id: 2
        } ],
        currentTab: 0
    },
    returnClick: function() {
        wx.reLaunch({
            url: "/hyb_jianzhi/index/index"
        });
    },
    onLoad: function(t) {
        this.getUserbaominglist(0);
    },
    getUserbaominglist: function(t) {
        var a = this;
        app.util.request({
            url: "entry/wxapp/Userbaominglist",
            data: {
                openid: wx.getStorageSync("openid"),
                n_id: t
            },
            success: function(t) {
                a.setData({
                    list: t.data.data
                });
            }
        });
    },
    swichNav: function(t) {
        var a = t.currentTarget.dataset.n_id;
        this.setData({
            currentTab: a
        }), this.getUserbaominglist(a);
    },
    callClick: function(t) {
        var a = t.currentTarget.dataset.phone;
        wx.makePhoneCall({
            phoneNumber: a
        });
    },
    cancelClick: function(t) {
        var a = this, e = t.currentTarget.dataset.b_id;
        app.util.request({
            url: "entry/wxapp/Baomingdel",
            data: {
                b_id: e
            },
            success: function(t) {
                a.getUserbaominglist(a.data.currentTab);
            }
        });
    },
    onShareAppMessage: function() {}
});