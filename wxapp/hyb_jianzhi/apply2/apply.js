const { $Message } = require('../../dist/base/index');
var app = getApp();

Page({
  data: {
    navArr: [{
      title: "已报名",
      n_id: 0
    }, {
      title: "已录用",
      n_id: 1
    }, {
      title: "已结算",
      n_id: 2
    }, {
      title: "全部",
      n_id: 3
    },],
    currentTab: 0,

  },
  returnClick: function () {
    wx.reLaunch({
      url: "/hyb_jianzhi/home/home"
    });
  },
  onLoad: function (t) {
    this.getUserbaominglist(0);
  },
  getUserbaominglist: function (t) {
    var a = this;
    app.util.request({
      url: "entry/wxapp/Userbaominglist",
      data: {
        openid: wx.getStorageSync("openid"),
        n_id: t + 1
      },
      success: function (t) {
        a.setData({
          list: t.data.data
        });
      }
    });
  },
  swichNav: function (t) {
    var a = t.currentTarget.dataset.n_id;
    this.setData({
      currentTab: a
    }), this.getUserbaominglist(a);
  },
  callClick: function (t) {
    var a = t.currentTarget.dataset.phone;
    wx.makePhoneCall({
      phoneNumber: a
    });
  },

  callClick11:function(t){
     var a = t.currentTarget.dataset.phone;
    wx.makePhoneCall({
      phoneNumber: a
    });
    //console.log(a);
  },
  cancelClick: function (t) {
    var a = this, e = t.currentTarget.dataset.b_id;
    app.util.request({
      url: "entry/wxapp/Baomingdel",
      data: {
        b_id: e
      },
      success: function (t) {
        a.getUserbaominglist(a.data.currentTab);
      }
    });
  },
  addmessageluqu: function (e) {
    var f_openid = e.currentTarget.dataset.f_openid;
    var b_openid = e.currentTarget.dataset.b_openid; 
    var b_id = e.currentTarget.dataset.b_id;
    var that = this;
    app.util.request({
      url: "entry/wxapp/AddMessage",
      data: {
        f_openid: f_openid,
        message_title: '提醒录用消息',
        b_openid: b_openid,
        is_pay: 0,
        b_id: b_id
      },
      success: function (data) {
        if (data.data.message == "success") {
          $Message({
            content: '操作成功',
            type: 'success'
          });
          setTimeout(() => {
            wx.navigateTo({
              url: "/hyb_jianzhi/apply2/apply",
            });
          }, 2000);
        }
        else
          $Message({
            content: '操作失败',
            type: 'error'
          });
      }
    });
  },
  addmessagepay: function (e) {
    var f_openid = e.currentTarget.dataset.f_openid;
    var b_openid = e.currentTarget.dataset.b_openid;
    var b_id = e.currentTarget.dataset.b_id;
    var that = this;
    app.util.request({
      url: "entry/wxapp/AddMessage",
      data: {
        f_openid: f_openid,
        message_title: '提醒付款消息',
        b_openid: b_openid,
        is_pay: 1,
        b_id: b_id
      },
      success: function (data) {
        if (data.data.message == "success") {
          $Message({
            content: '操作成功',
            type: 'success'
          });
          setTimeout(() => {
            wx.navigateTo({
              url: "/hyb_jianzhi/apply2/apply",
            });
          }, 2000);
        }
        else
          $Message({
            content: '操作失败',
            type: 'error'
          });
      }
    });
  },
  comment:function(e){
    var company_openid = e.currentTarget.dataset.company_openid;
    var f_id = e.currentTarget.dataset.f_id;
    var b_id = e.currentTarget.dataset.b_id;
    wx.navigateTo({
      url: '/hyb_jianzhi/pingjia/index?company_openid=' + company_openid+'&f_id='+f_id+'&b_id='+b_id,
    })
  },
  onPullDownRefresh: function () {
    this.onLoad();
    wx.stopPullDownRefresh();
  }

});