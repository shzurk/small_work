var app = getApp();
Page({

  data: {},
  onLoad: function (e) {
    var message_id = e.message_id
    var that = this;

    app.util.request({
      url: "entry/wxapp/GetMessageById",
      data: {
        message_id: message_id
      },
      success: function (a) {
        that.setData({
          list: a.data.data
        })
      },
      error: function () {

      }
    });
  }

})