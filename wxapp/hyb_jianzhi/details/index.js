var app = getApp();

Page({
  data: {

  },

  onLoad:function(e){
    var user_openid = e.user_openid;
    var f_id = e.f_id;
    var that=this;
    app.util.request({
      url: "entry/wxapp/Details",
      data: {
        user_openid: user_openid,
        f_id:f_id
      },
      success: function (a) {
        that.setData({
          list: a.data.data
        });
      }
    });

  },
  returnClick: function () {
    wx.reLaunch({
      url: "/hyb_jianzhi/home/home"
    });
  },

 
});
