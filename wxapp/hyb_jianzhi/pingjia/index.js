const { $Message } = require('../../dist/base/index');
const app = getApp()

Page({
  data: {
    total_type: 0,
    work_type: 0,
    describe_type: 0,
    efficiency_type: 0,
    comment_content:''

  },
  onChange1(e) {
    const index = e.detail.index;
    this.setData({
      'total_type': index
    })
  },
  onChange2(e) {
    const index = e.detail.index;
    this.setData({
      'work_type': index
    })
  },
  onChange3(e) {
    const index = e.detail.index;
    this.setData({
      'describe_type': index
    })
  },
  
  onChange4(e) {
    const index = e.detail.index;
    this.setData({
      'efficiency_type': index
    })
  },
  onChange5(e) {
    const index = e.detail.index;
    this.setData({
      'onChange5': index
    })
  },
  bindcontent:function(e){
    this.setData({
      comment_content: e.detail.value
    })
  },
  onLoad:function(e){
    var company_openid = e.company_openid;
    var f_id = e.f_id;
    var b_id = e.b_id;
    this.setData({
      company_openid: company_openid,
      f_id: f_id,
      b_id: b_id,
    });
    var that = this;
    app.util.request({
      url: "entry/wxapp/GetWorkById",
      data: {
        f_id:f_id
      },
      success: function (a) {
        that.setData({
          list: a.data.data
        })
      },
      error: function () {

      }
    });
  },
  submitcomment:function(e){
    if (this.data.comment_content == '') {
      $Toast({
        content: '评价内容不能为空',
        type: 'warning'
      });
      return;
    }
    var that = this;
    app.util.request({
      url: "entry/wxapp/UserComment",
      data: {
        company_openid: that.data.company_openid,
        user_openid: wx.getStorageSync('openid'),
        f_id: that.data.f_id,
        total_type: that.data.total_type,
        work_type: that.data.work_type,
        describe_type: that.data.describe_type,
        efficiency_type: that.data.efficiency_type,
        comment_content: that.data.comment_content,
        b_id: that.data.b_id
      },
      success: function (data) {
        if (data.data.message == "success") {
          $Message({
            content: '操作成功',
            type: 'success'
          });
          setTimeout(() => {
            wx.navigateTo({
              url: "/hyb_jianzhi/apply2/apply",
            });
          }, 2000);
        }
        else
          $Message({
            content: '操作失败',
            type: 'error'
          });
      }
    });
  },
  returnClick: function () {
    wx.reLaunch({
      url: "/hyb_jianzhi/home/home"
    });
  },

});

