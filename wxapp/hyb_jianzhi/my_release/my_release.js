var app = getApp();

Page({
    data: {
        showmodal: !1,
        array: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40 ],
        zhid_index: 0,
        moneyarr: "",
        money: "",
        which: "",
        pindao: "",
        openid: "",
        taocan: [],
        currentTab: 0
    },
    chakan: function(a) {
        var t = a.currentTarget.dataset.id;
        wx.navigateTo({
            url: "/hyb_jianzhi/detail/detail?f_id=" + t + "&my=my"
        });
    },
    del_fabu: function(t) {
        var e = this;
        wx.showModal({
            title: "提示",
            content: "确定删除吗？",
            success: function(a) {
                a.confirm && app.util.request({
                    url: "entry/wxapp/Fabudel",
                    data: {
                        f_id: t.currentTarget.dataset.f_id
                    },
                    success: function(a) {
                        e.getUserjianzhi();
                    }
                });
            }
        });
    },
    zhiding: function(a) {
        var t = a.currentTarget.dataset.id, e = a.currentTarget.dataset.pindao;
        this.setData({
            showmodal: !0,
            which: t,
            pindao: e
        });
    },
    bindChange: function(a) {
        var t = a.detail.value, e = this.data.array[t], n = this.data.money;
        n *= e, this.setData({
            zhid_index: a.detail.value,
            moneyarr: n
        });
    },
    cancel_box: function() {
        this.setData({
            showmodal: !1
        });
    },
    apply1Click: function(a) {
        wx.navigateTo({
            url: "/hyb_jianzhi/apply1/apply1?f_id=" + a.currentTarget.dataset.f_id
        });
    },
    formSubmit: function(a) {
        var t = this, e = a.detail.value;
        console.log(e), wx.showModal({
            title: "提示",
            content: "确认支付？",
            showCancel: !0,
            success: function(a) {
                a.confirm ? app.util.request({
                    url: "entry/wxapp/Pay",
                    data: {
                        openid: e.openid,
                        zhiding_money: e.zhiding_money
                    },
                    header: {
                        "Content-Type": "application/json"
                    },
                    success: function(a) {
                        wx.requestPayment({
                            timeStamp: a.data.timeStamp,
                            nonceStr: a.data.nonceStr,
                            package: a.data.package,
                            signType: a.data.signType,
                            paySign: a.data.paySign,
                            success: function(a) {
                                app.util.request({
                                    url: "entry/wxapp/Zdsave",
                                    data: e,
                                    success: function(a) {
                                        wx.showToast({
                                            title: "置顶成功",
                                            success: function(a) {
                                                setTimeout(function() {
                                                    t.getUserjianzhi();
                                                }, 1e3);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                }) : a.cancel && console.log("用户点击取消");
            }
        }), this.cancel_box();
    },
    onLoad: function(a) {
        var t = this, e = wx.getStorageSync("openid");
        t.setData({
            openid: e
        }), t.getBase(), t.getUserjianzhi();
    },
    getBase: function() {
        var t = this;
        app.util.request({
            url: "entry/wxapp/Base",
            success: function(a) {
                t.setData({
                    base: a.data.data,
                    moneyarr: a.data.data.zd_money,
                    money: a.data.data.zd_money
                });
            }
        });
    },
    getUserjianzhi: function() {
        var t = this;
        app.util.request({
            url: "entry/wxapp/Userjianzhi",
            data: {
                openid: wx.getStorageSync("openid")
            },
            success: function(a) {
                console.log(a.data.data), t.setData({
                    list: a.data.data
                });
            }
        });
    },
    jumpurl: function (e) {
      var id = e.currentTarget.id;
      // var app = getApp();
      // app.requestDetailid=id;
      console.log(id);
      wx.navigateTo({
        url: id
      })
    },
    returnClick: function () {
      wx.reLaunch({
        url: "/hyb_jianzhi/home/home"
      });
    },
    onShareAppMessage: function() {}
});