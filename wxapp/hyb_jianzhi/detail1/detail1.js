const { $Message } = require('../../dist/base/index');
const { $Toast } = require('../../dist/base/index');
var app = getApp();

Page({
  data: {
    money: '',
    title: '',
    // tab切换  

    currentTab: 0,
    starIndex1: 0,
    starIndex2: 0,
    starIndex3: 0,
    starIndex5: 5,
    ludisabled: false,
    paydisable: false,
    // 兼职经历
    current: 2,
    verticalCurrent: 0
    // end
  },
  // 兼职经历
  handleClick() {
    const addCurrent = this.data.current + 1;
    const current = addCurrent > 2 ? 0 : addCurrent;
    this.setData({
      'current': current
    })
  }
  // end
  , swichNav: function (e) {

    console.log(e);
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current,
      })
    }
  },

  swiperChange: function (e) {

    console.log(e);

    this.setData({

      currentTab: e.detail.current,

    })

  },
  onLoad: function (a) {
    this.setData({
      f_id: a.f_id,
      f_name: a.f_name,
      user_openid: a.user_openid
    });
    var t = a.b_id;
    this.getJianzhibaomingxq(t);
    var that = this;
    app.util.request({
      url: "entry/wxapp/GetWorkById",
      data: {
        f_id: a.f_id
      },
      success: function (a) {
        that.setData({
          worklist: a.data.data
        });
      }
    });
    app.util.request({
      url: "entry/wxapp/getCompanyComment",
      data: {
        openid: that.data.user_openid
      },
      success: function (a) {
        that.setData({
          comment: a.data.data.comment,
          school: a.data.data.school,
          isComment: a.data.data.isComment
        });
      }
    });
  },
  getJianzhibaomingxq: function (a) {
    var t = this;
    app.util.request({
      url: "entry/wxapp/Jianzhibaomingxq",
      data: {
        b_id: a
      },
      success: function (a) {
        t.setData({
          list: a.data.data
        });
      }
    });
  },
  cancelClick: function (a) {
    var t = a.currentTarget.dataset.id;
    var that = this;
    wx.navigateTo({
      url: "/hyb_jianzhi/quxiaoluyong/index?b_id=" + t + "&f_id=" + that.data.f_id + "&f_name=" + that.data.f_name
    });
  },
  luClick: function (a) {
    var t = this, i = a.currentTarget.dataset.id;
    t.setData({
      ludisabled: true
    })
    console.log(i), app.util.request({
      url: "entry/wxapp/IsEmployment",
      data: {
        b_id: i,
        is_employment: 1
      },
      success: function (data) {
        if (data.data.message == "success") {
          $Message({
            content: '操作成功',
            type: 'success'
          });
          t.Deletecache();
          setTimeout(() => {
            wx.navigateTo({
              url: "/hyb_jianzhi/apply1/apply1?f_id=" + t.data.f_id + "&f_name=" + t.data.f_name,
            });
          }, 1500);
        }
        else
          $Message({
            content: '操作失败',
            type: 'error'
          });
      }
    });
    t.setData({
      ludisabled: false
    })
  },
  bindmoney: function (e) {
    this.setData({
      money: e.detail.value
    })
  },
  bindtitle: function (e) {
    this.setData({
      title: e.detail.value
    })
  },
  payCompany: function (event) {
    if (this.data.money == '') {
      $Toast({
        content: '薪酬不能为空',
        type: 'warning'
      });
      return;
    }
    if (this.data.title == '') {
      $Toast({
        content: '备注不能为空',
        type: 'warning'
      });
      return;
    }
    var orderid = Math.random();
    var user_openid = event.currentTarget.dataset.openId;
    var company_openid = wx.getStorageSync("openid");
    var that = this;
    that.setData({
      paydisable: true
    })
    app.util.request({
      'url': 'entry/wxapp/PayCompany', //调用wxapp.php中的doPagePay方法获取支付参数
      data: {
        money: that.data.money,
        openid: company_openid,
        f_id: that.data.f_id,
        user_openid: user_openid,
        title: that.data.title

      },
      'cachetime': '0',
      success(res) {
        if (res.data && res.data.data && !res.data.errno) {
          //发起支付
          wx.requestPayment({
            'timeStamp': res.data.data.timeStamp,
            'nonceStr': res.data.data.nonceStr,
            'package': res.data.data.package,
            'signType': 'MD5',
            'paySign': res.data.data.paySign,
            'success': function (res) {
              $Message({
                content: '支付成功',
                type: 'success'
              });
              that.Deletecache();
              setTimeout(() => {
                wx.navigateTo({
                  url: "/hyb_jianzhi/apply1/apply1?f_id=" + that.data.f_id + "&f_name=" + that.data.f_name,
                });
              }, 1500);
            },
            'fail': function (res) {
              $Message({
                content: '支付失败',
                type: 'error'
              });
            }
          })
        }
      },
      fail(res) {
        wx.showModal({
          title: '系统提示',
          content: res.data.message ? res.data.message : '错误',
          showCancel: false,
          success: function (res) {
            if (res.confirm) {
              //backApp()
            }
          }
        })
      }
    });
    that.setData({
      paydisable: false
    })
  },
  returnClick: function () {
    wx.reLaunch({
      url: "/hyb_jianzhi/home/home"
    });
  },

  Deletecache:function(){ 
    var a={};
    a.f_id=this.data.f_id;
    a.f_name=this.data.f_name;
    var pages = getCurrentPages();
    if (pages.length > 1) {
      var prePage = pages[pages.length - 2];
      prePage.onLoad(a);
    }
    if (pages.length > 2) {
      var preprePage = pages[pages.length - 3];
      preprePage.onLoad()
    }
  }
});