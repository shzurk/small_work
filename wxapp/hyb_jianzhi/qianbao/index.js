//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
  },

  onLoad:function(e){
    var t = this;
    app.util.request({
      url: "entry/wxapp/User",
      data: {
        openid: wx.getStorageSync("openid")
      },
      success: function (a) {
        var newa = a.data.data;
        newa.u_money = Math.floor(newa.u_money * 100) / 100; 
        t.setData({
          user: newa
        });
      },
      error: function () {

      }
    });
  },
  putmoney:function(e){
    var u_money = e.currentTarget.dataset.u_money;
    wx.navigateTo({
      url: '/hyb_jianzhi/putmoney/putmoney?u_money=' + u_money,
    })
  }
  ,
  moneylist: function (e) {
    wx.navigateTo({
      url: '/hyb_jianzhi/zhanghumingxi/index',
    })
  }
  

});

