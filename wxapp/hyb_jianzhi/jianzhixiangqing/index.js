var app = getApp();

Page({
  data: {
    qu: "",
    my: "",
    total:[],
    person:0,
    position:0
  },
  onLoad: function (a) {
    var t = a.f_id, e = a.my;
    this.setData({
      my: e
    }), this.getJianzhixq(t);
  },
  getCompany:function(){
    console.log(this.data.jianzhi_infoxq.s_id);
    var s_id = this.data.jianzhi_infoxq.s_id;
    wx.redirectTo({
      url: "/hyb_jianzhi/qiyexiangqin/mainpage?s_id="+s_id
    });
  },
  getJianzhixq: function (a) {
    var i = this;
    app.util.request({
      url: "entry/wxapp/Jianzhi_infoxq",
      data: {
        f_id: a
      },
      success: function (a) {
        var t = a.data.data.f_address;
        var e = new RegExp(/[市|区|县]/);
        /**获得用户位置 */
        var longitude = wx.getStorageSync('longitude')
        var latitude = wx.getStorageSync('latitude')
        var s_position = i.GetDistance(latitude,longitude,a.data.data.f_lat,a.data.data.f_lon);
        t = t.split(e), i.setData({
          jianzhi_infoxq: a.data.data,
          qu: t[1],
          person: a.data.data.jianzhi_count[0].count,
          position: s_position
        });
        if (a.data.data.total_type!=0){
          var test=[];
          for (var j = 0; j < a.data.data.total_type;j++)
            test.push(j);
          i.setData({
            total:test
          })
            
        }
      }
    });
  },
  enrollClick: function (t) {
    console.log(wx.getStorageSync("openid")), console.log(t.currentTarget.dataset.f_openid),
      app.util.request({
        url: "entry/wxapp/User",
        data: {
          openid: wx.getStorageSync("openid")
        },
        success: function (a) {
          null == a.data.data.u_bname ? wx.showModal({
            title: "提示",
            content: "请先完善信息",
            success: function (a) {
              a.confirm && wx.navigateTo({
                url: "/hyb_jianzhi/edit/edit"
              });
            }
          }) : "待审核" == a.data.data.u_typs ? wx.showToast({
            title: "请等待审核",
            image: "/hyb_jianzhi/images/errors.png"
          }) : wx.getStorageSync("openid") == t.currentTarget.dataset.f_openid ? wx.showToast({
            title: "您是发布者",
            image: "/hyb_jianzhi/images/errors.png"
          }) : "不限" != t.currentTarget.dataset.sex && a.data.data.u_sex != t.currentTarget.dataset.sex ? wx.showToast({
            title: "不符合需求",
            image: "/hyb_jianzhi/images/errors.png"
          }) : app.util.request({
            url: "entry/wxapp/Userbaiming",
            data: {
              openid: wx.getStorageSync("openid"),
              f_id: t.currentTarget.dataset.f_id
            },
            success: function (a) {
              0 == a.data.data ? wx.reLaunch({
                url: "/hyb_jianzhi/send/send?f_id=" + t.currentTarget.dataset.f_id
              }) : wx.showToast({
                title: "您已报名",
                image: "/hyb_jianzhi/images/errors.png"
              });
            }
          });
        }
      });
  },
  returnClick: function () {
    wx.redirectTo({
      url: "/hyb_jianzhi/index/index"
    });
  },
  mapClick: function (a) {
    wx.openLocation({
      latitude: parseFloat(a.currentTarget.dataset.f_lat),
      longitude: parseFloat(a.currentTarget.dataset.f_lon),
      address: a.currentTarget.dataset.f_address,
      scale: 22
    });
  },
  callphone:function(){
    var i = this;
    var phone = i.data.jianzhi_infoxq.f_fuzetel;
    wx.makePhoneCall({
      phoneNumber: phone
    })
  },
  Rad : function (d){
    return d * Math.PI / 180.0;//经纬度转换成三角函数中度分表形式。
  },
  //计算距离，参数分别为第一点的纬度，经度；第二点的纬度，经度 
  GetDistance: function(lat1, lng1, lat2, lng2) {
    var radLat1 = this.Rad(lat1);
    var radLat2 = this.Rad(lat2);
    var a = radLat1 - radLat2;
    var b = this.Rad(lng1) - this.Rad(lng2);
    var s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
    s = s * 6378.137; // EARTH_RADIUS; s = Math.round(s * 10000) / 10000; //输出为公里 //s=s.toFixed(4); 
    return Math.round(s); 
  },
  onShareAppMessage: function () {
    wx.showShareMenu({
      withShareTicket: true
    })
  },
  callTel:function(){
    wx.makePhoneCall({
      phoneNumber: '0991-6634545'
    })
  }

});