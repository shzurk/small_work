var app = getApp(), city = require("../../utils/city.js");

Page({
    data: {
        currentCity: "定位中...",
        letterArr: [],
        city1: []
    },
    onLoad: function(t) {
        var a = city.searchLetter, e = city.cityList(), i = a;
        this.setData({
            city1: e,
            letterArr: i
        });
    },
    confirmClick: function(t) {
        var a = t.target.dataset.a_id;
        wx.setStorage({
            key: "city",
            data: a
        });
        var e = new RegExp("[一-龥]+", "g");
        a.search(e) || wx.reLaunch({
            url: "/hyb_jianzhi/index/index?a_id=" + a
        });
    },
    anchorClick: function(t) {
        var a = t.currentTarget.dataset.a_id;
        this.setData({
            toView: a
        });
    }
});