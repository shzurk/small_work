const { $Toast } = require('../../dist/base/index');
const { $Message } = require('../../dist/base/index');
var app = getApp();

Page({
  data: {
    money:null,
    suc:false,
    u_money:null,
    isdis:false
  },
  onLoad: function (e) {
    var u_money = e.u_money;
    this.setData({
      u_money: u_money
    })
  },
  bindmoney:function(e){
    this.setData({
      money: e.detail.detail.value
    })
  },
  submituser:function(e){
    if (this.data.money == null) {
      $Toast({
        content: '钱数不能为空',
        type: 'warning'
      });
      return;
    }
    if (this.data.money >this.data.u_money) {
      $Toast({
        content: '金额不能超过钱包金额总数',
        type: 'warning'
      });
      return;
    }
    var that = this;
    that.setData({
      suc:false,
      isdis:true
    });
    
    app.util.request({
      url: "entry/wxapp/PutMoney",
      data: {
        money:that.data.money,
        openid: wx.getStorageSync("openid")
      },
      success: function (data) {
        $Toast.hide();
        console.log(data.data);
        if (data.data.message == "success") {
          $Message({
            content: '提现成功',
            type: 'success'
          });
          that.setData({
            u_money: that.data.u_money-that.data.money
          });

            wx.navigateTo({
              url: '/hyb_jianzhi/qianbao/index',
            });

        }
        else{
          $Message({
            content: '提现失败',
            type: 'error'
          });

          that.setData({
            isdis:false
          })
        }
      },
      error: function () {

      }
    });
  }

})