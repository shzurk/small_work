//index.js
Page({
  data: {
    isGet: false,
    sec: 60
  },
  getCode() {
    var self = this
    self.setData({ isGet: true })
    var remain = 60;
    var time = setInterval(function () {
      if (remain == 1) {
        clearInterval(time)
        self.setData({
          sec: 60,
          isGet: false
        })
        return false
      }
      remain--;
      self.setData({
        sec: remain
      })
    }, 1000)
  }
})
