var app = getApp();

Page({
  data: {
    openid: "",
    start: "1900-01-01",
    end: "2040-12-31",
    array: ["150", "151", "152", "153", "154", "155", "156", "157", "158", "159", "160", "161", "162", "163", "164", "165", "166", "167", "168", "169", "170", "171", "172", "173", "174", "175", "176", "177", "178", "179", "180", "181", "182", "183", "184", "185", "186", "187", "188", "189", "190以上"],
    Nation: ["汉族", "蒙古族", "回族", "藏族", "维吾尔族", "苗族", "彝族", "壮族", "布依族", "朝鲜族", "满族", "侗族", "瑶族", "白族", "土家族",
      "哈尼族", "哈萨克族", "傣族", "黎族", "傈僳族", "佤族", "畲族", "高山族", "拉祜族", "水族", "东乡族", "纳西族", "景颇族", "柯尔克孜族",
      "土族", "达斡尔族", "仫佬族", "羌族", "布朗族", "撒拉族", "毛南族", "仡佬族", "锡伯族", "阿昌族", "普米族", "塔吉克族", "怒族", "乌孜别克族",
      "俄罗斯族", "鄂温克族", "德昂族", "保安族", "裕固族", "京族", "塔塔尔族", "独龙族", "鄂伦春族", "赫哲族", "门巴族", "珞巴族", "基诺族"],
    index: 0,
    cursor: 0,
    i: 0,
    display: "none",
    sex: 0,
    userDate: "请选择出生日期",
    userHeight: "请选择身高",
    userNation: "汉族",
    userJiaoyu: "在读",
    phoneNum: "",
    code: "",
    smscode: "",
    otherInfo: "",
    send: !1,
    sendMsg: "发送验证码",
    alreadySend: !1,
    second: 60,
    Xinmingdisabled: '',
    Xinbiedisabled: '',
    Chushendisabled: '',
    Zubiedisabled: '',
    user_header:'../images/add.png',
    input_user_header:''
  },
  onLoad: function (a) {
    var e = wx.getStorageSync("openid");
    this.setData({
      openid: e
    }), this.getUser(e);
    var t = this;
    app.util.request({
      url: "entry/wxapp/url",
      success: function (a) {
        t.setData({
          url: a.data
        });
      }
    })
  },
  getUser: function (a) {
    var e = this;
    f = app.siteInfo.uniacid;
    
    e.setData({
      uniacid: f
    })
    console.log(f);
    app.util.request({
      url: "entry/wxapp/User",
      data: {
        openid: a,
      },
      success: function (a) {
        if (a.data.data.u_bname != null) {
          e.setData({
            Xinmingdisabled: "disabled"
          })
        }

        if (a.data.data.u_sex != "暂无") {
          e.setData({
            Xinbiedisabled: "disabled"
          })
        }

        if (a.data.data.u_chusheng != null) {
          e.setData({
            Chushendisabled: "disabled"
          })
        }

        if (a.data.data.u_nation != null) {
          e.setData({
            Zubiedisabled: "disabled"
          })
        }


        if (a.data.data.u_relheader != null){
          e.setData({
            user_header: a.data.data.u_relheader
          })
        }

        

        e.setData({
          user: a.data.data
        }), a.data.data.u_content && e.setData({
          cursor: a.data.data.u_content.length
        }), null != a.data.data.u_chusheng && null != a.data.data.u_shengao && e.setData({
          userDate: a.data.data.u_chusheng,
          userHeight: a.data.data.u_shengao,
          userNation: a.data.data.u_nation
        }), "在读" == a.data.data.u_jiaoyu ? e.setData({
          userJiaoyu: a.data.data.u_jiaoyu,
          i: 0
        }) : "已毕业" == a.data.data.u_jiaoyu && e.setData({
          userJiaoyu: a.data.data.u_jiaoyu,
          i: 1
        }), "男" == a.data.data.u_sex ? e.setData({
          sex: 0
        }) : "女" == a.data.data.u_sex && e.setData({
          sex: 1
        });
      }
    });
  },
  inputPhoneNum: function (a) {
    var e = a.detail.value;
    11 === e.length ? this.checkPhoneNum(e) && this.setData({
      tel: e,
      send: !0
    }) : this.setData({
      phoneNum: "",
      send: !1
    });
  },
  checkPhoneNum: function (a) {
    return !!/^1[3|4|5|7|8][0-9]\d{4,8}$/.test(a) || (wx.showToast({
      title: "手机号不正确",
      image: "/hyb_jianzhi/images/errors.png"
    }), !1);
  },
  sendMsg: function () {
    var e = this;
    app.util.request({
      url: "entry/wxapp/SendSms",
      data: {
        tel: e.data.tel
      },
      header: {
        "content-type": "application/json"
      },
      success: function (a) {
        e.setData({
          smscode: a.data.data.code
          //smscode:1
        });
      }
    }), this.setData({
      alreadySend: !0,
      send: !1
    }), this.timer();
  },
  timer: function () {
    var s = this;
    new Promise(function (a, e) {
      var t = setInterval(function () {
        s.setData({
          second: s.data.second - 1
        }), s.data.second <= 0 && (s.setData({
          second: 60,
          alreadySend: !1,
          send: !0
        }), a(t));
      }, 1e3);
    }).then(function (a) {
      clearInterval(a);
    });
  },
  sex0Click: function () {
    this.setData({
      sex: 0
    });
  },
  sex1Click: function () {
    this.setData({
      sex: 1
    });
  },
  readClick: function () {
    this.setData({
      i: 1,
      userJiaoyu: "已毕业"
    });
  },
  readClick1: function () {
    this.setData({
      i: 0,
      userJiaoyu: "在读"
    });
  },
  dateChange: function (a) {
    var e = a.detail.value;
    this.setData({
      userDate: e
    });
  },
  indexChange: function (a) {
    var e = a.detail.value, t = this.data.array;
    this.setData({
      userHeight: t[e]
    });
  },
  nationChange: function (a) {
    var e = a.detail.value, t = this.data.Nation;
    this.setData({
      userNation: t[e]
    });
  },
  numInp: function (a) {
    var e = a.detail.cursor;
    this.setData({
      cursor: e
    });
  },
  submitClick: function (a) {
    wx.showLoading({
      title: '请稍后',
      mask: true
    })
    var e = a.detail.value;
    e.userDate = this.data.userDate, e.userJiaoyu = this.data.userJiaoyu, console.log(e);
    var t = new RegExp("^[一-龥豈-鶴·s]{2,20}$", "g");
    "" == e.userName ? wx.showToast({
      title: "请输入姓名",
      image: "/hyb_jianzhi/images/errors.png"
    }) : "" == e.userName || t.test(e.userName) ? "请选择出生日期" == e.userDate ? wx.showToast({
      title: "请选择出生日期",
      image: "/hyb_jianzhi/images/errors.png"
    }) : "" == e.telphone ? wx.showToast({
      title: "请输入手机号",
      image: "/hyb_jianzhi/images/errors.png"
    }) : app.util.request({
      url: "entry/wxapp/Userxin",
      data: e,
      success: function (a) {
        wx.showToast({
          title: "简历提交成功",
          image: "/hyb_jianzhi/images/errors.png",
          success: function (a) {
            wx.hideLoading();
            setTimeout(function () {
              wx.reLaunch({
                url: "/hyb_jianzhi/usercenter/index"
              });
            }, 1e3);
          }
        });
      }
    }) : wx.showToast({
      title: "请输入正确姓名",
      image: "/hyb_jianzhi/images/errors.png"
    });
  },
  yanzhengma: function (a) {
    a.detail.value != this.data.smscode && wx.showToast({
      title: "验证码输入错误",
      image: "/hyb_jianzhi/images/errors.png"
    });
  },
  emailReg: function (a) {
    var e = new RegExp("^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(.[a-zA-Z0-9-]+)*.[a-zA-Z0-9]{2,6}$", "g"), t = a.detail.value;
    e.test(t) || wx.showToast({
      title: "请输入正确邮箱",
      image: "/hyb_jianzhi/images/errors.png"
    });
  },
  qqReg: function (a) {
    var e = new RegExp("[1-9][0-9]{4,14}", "g"), t = a.detail.value;
    e.test(t) || wx.showToast({
      title: "请输入正确QQ",
      image: "/hyb_jianzhi/images/errors.png"
    });
  },
  onShareAppMessage: function () { },
  upload_header:function(){
    var n = this, u = n.data.uniacid;
    wx.chooseImage({
      success: function (res) {
        var tempFilePaths = res.tempFilePaths
        wx.uploadFile({
          url: n.data.url + "app/index.php?i=" + u + "&c=entry&a=wxapp&do=upload&m=hyb_jianzhi",
          filePath: tempFilePaths[0],
          name: "upfile",
          formData: {
            openid: wx.getStorageSync("openid")
          },
          success: function (res) {
            var data = res.data
            n.setData({
              user_header:data,
              input_user_header:data
            });
            //do something
          }
        })
      }
    })
  }
});