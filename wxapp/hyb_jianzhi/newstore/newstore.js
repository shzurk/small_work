var app = getApp();

Page({
    data: {
        openid: "",
        uniacid: "",
        uplogo: "",
        uplogos: "",
        imglist: [],
        typs: "",
        loc_address: "请选择",
        loc_lon: "",
        loc_lat: "",
        no_msg:0,
      is_submit:false,
      current: "mine"
    },
    onLoad: function(a) {
        var t = this, e = app.siteInfo.uniacid, i = wx.getStorageSync("openid");
        t.setData({
            openid: i,
            uniacid: e
        }), app.util.request({
            url: "entry/wxapp/url",
            success: function(a) {
                t.setData({
                    url: a.data
                });
            }
        }), app.util.request({
            url: "entry/wxapp/Shangjiainfo",
            data: {
                openid: wx.getStorageSync("openid")
            },
            success: function(a) {
                0 != a.data.data && t.setData({
                    shangjiainfo: a.data.data,
                    loc_address: a.data.data.s_address,
                    loc_lon: a.data.data.s_lon,
                    loc_lat: a.data.data.s_lat,
                    uplogo: a.data.data.s_thumb,
                    uplogos: a.data.data.s_yinye,
                    imglist: a.data.data.s_imgpath
                }), 0 == a.data.data ? t.setData({
                    typs: "入驻"
                }) : t.setData({
                    typs: "编辑"
                });
            }
        }), t.getBase();

      app.util.request({
        url: "entry/wxapp/GetNolookMsg",
        data: {
          openid: wx.getStorageSync("openid")
        },
        success: function (a) {
          t.setData({
            no_msg:a.data.data
          });
         
        },
        error: function () {
        }
      });

    },
    getBase: function() {
        var t = this;
        app.util.request({
            url: "entry/wxapp/Base",
            success: function(a) {
                t.setData({
                    base: a.data.data
                });
            },
            fail: function(a) {
                console.log(a);
            }
        });
    },
    open_map_chonse: function(e) {
        var t = this;
        console.log(e);
        // wx.chooseLocation({
            // success: function(a) {
                t.setData({
                  loc_address: e.detail.value
                });
            // }
        //});
    },
    uploadImg: function() {
        var e = this, i = e.data.uniacid;
        wx.chooseImage({
            count: 1,
            sizeType: [ "original", "compressed" ],
            sourceType: [ "album", "camera" ],
            success: function(a) {
                var t = a.tempFilePaths[0];
                e.setData({
                    upvimage: t
                }), wx.uploadFile({
                    url: e.data.url + "app/index.php?i=" + i + "&c=entry&a=wxapp&do=upload&m=hyb_jianzhi",
                    filePath: t,
                    name: "upfile",
                    formData: {},
                    success: function(a) {
                        console.log(a.data), e.setData({
                            uplogo: a.data
                        });
                    }
                });
            }
        });
    },
    uploadImg2: function() {
        var e = this, i = e.data.uniacid;
        wx.chooseImage({
            count: 1,
            sizeType: [ "original", "compressed" ],
            sourceType: [ "album", "camera" ],
            success: function(a) {
                var t = a.tempFilePaths[0];
                e.setData({
                    upvimage: t
                }), wx.uploadFile({
                    url: e.data.url + "app/index.php?i=" + i + "&c=entry&a=wxapp&do=upload&m=hyb_jianzhi",
                    filePath: t,
                    name: "upfile",
                    formData: {},
                    success: function(a) {
                        console.log(a.data), e.setData({
                            uplogos: a.data
                        });
                    }
                });
            }
        });
    },
    upimage: function() {
        var e = this;
        wx.chooseImage({
            count: 6 - e.data.imglist.length,
            sizeType: [ "compressed" ],
            sourceType: [ "album", "camera" ],
            success: function(a) {
                a.tempFilePaths;
                var t = a.tempFilePaths.length;
                e.uploadDIY(a.tempFilePaths, 0, 0, 0, t), console.log(e.data.imglist);
            }
        });
    },
    uploadDIY: function(a, t, e, i, s) {
        var o = this, n = this, u = n.data.uniacid, l = n.data.imglist;
        wx.uploadFile({
            url: n.data.url + "app/index.php?i=" + u + "&c=entry&a=wxapp&do=upload&m=hyb_jianzhi",
            filePath: a[i],
            name: "upfile",
            formData: {
                openid: wx.getStorageSync("openid")
            },
            success: function(a) {
                console.log(a.data), t++, l.push(a.data), n.setData({
                    imglist: l
                });
            },
            fail: function(a) {
                e++;
            },
            complete: function() {
                ++i == s ? (wx.hideLoading(), console.log("总共" + t + "张上传成功," + e + "张上传失败！")) : o.uploadDIY(a, t, e, i, s);
            }
        });
    },
    delPic: function(a) {
        var e = this, i = a.currentTarget.dataset.index;
        wx.showModal({
            title: "提示",
            content: "是否删除该图片",
            success: function(a) {
                if (a.confirm) {
                    var t = e.data.imglist;
                    t.splice(i, 1), e.setData({
                        imglist: t
                    });
                }
            }
        });
    },
    formSubmit: function(a) {
      var that = this;
      wx.showLoading({
        title: '请稍后',
        mask:true
      })
        var t = a.detail.value;
        if (t.imgpath = this.data.imglist, console.log(t), "" == t.store_name) wx.showToast({
            title: "请输入公司名称",
            image: "/hyb_jianzhi/images/errors.png",
            duration: 2e3
        }); else if ("" == t.name) wx.showToast({
            title: "请输入联系人",
            image: "/hyb_jianzhi/images/errors.png",
            duration: 1e3
        }); else if ("" == t.tel) wx.showToast({
            title: "请输入联系电话",
            image: "/hyb_jianzhi/images/errors.png",
            duration: 1e3
        }); else if (11 != t.tel.length) wx.showToast({
            title: "联系电话长度为11位",
            image: "/hyb_jianzhi/images/errors.png"
        }); else if ("请选择" == t.loc_address) wx.showToast({
            title: "请选择公司地址",
            image: "/hyb_jianzhi/images/errors.png",
            duration: 1e3
        }); else if ("" == t.mendian || 0 == t.mendian.length) wx.showToast({
            title: "请上传门店照片",
            image: "/hyb_jianzhi/images/errors.png",
            duration: 1e3
        }); else if ("" == t.yingyezhizhao || 0 == t.mendian.yingyezhizhao) wx.showToast({
            title: "请上传营业执照",
            image: "/hyb_jianzhi/images/errors.png",
            duration: 1e3
        }); else {
      
          that.setData({
            is_submit:true
          })
            var e = t.ruzhu;
            "入驻" == t.typs ? 0 == t.ruzhu ? app.util.request({
                url: "entry/wxapp/Shopruzhu",
                data: t,
                success: function(a) {
                    wx.hideLoading();
                    wx.showToast({
                        title: "入驻成功!"
                    }),
                  that.setData({
                    is_submit: true
                      }), wx.redirectTo({
                        url: "/hyb_jianzhi/newstore/newstore"
                      });
                }
            }) : app.util.request({
                url: "entry/wxapp/Pay",
                data: {
                    openid: t.openid,
                    zhiding_money: e
                },
                header: {
                    "Content-Type": "application/json"
                },
                success: function(a) {
                    wx.requestPayment({
                        timeStamp: a.data.timeStamp,
                        nonceStr: a.data.nonceStr,
                        package: a.data.package,
                        signType: a.data.signType,
                        paySign: a.data.paySign,
                        success: function(a) {
                            app.util.request({
                                url: "entry/wxapp/Shopruzhu",
                                data: t,
                                success: function(a) {
                                    wx.hideLoading();
                                    wx.showToast({
                                        title: "入驻成功!"
                                  }),
                                   that.setData({
                                    is_submit: true
                                      }), wx.redirectTo({
                                        url: "/hyb_jianzhi/newstore/newstore"
                                      });
                                }
                            });
                        }
                    });
                }
            }) : "编辑" == t.typs && app.util.request({
                url: "entry/wxapp/Shopruzhu",
                data: t,
                success: function(a) {
                    wx.hideLoading();
                    wx.showToast({
                        title: "请等待审核"
                    }), setTimeout(function() {
                      that.setData({
                        is_submit: false
                      })
                        wx.redirectTo({
                            url: "/hyb_jianzhi/newstore/newstore"
                        });
                    }, 1e3);
                }
            });
        }
    },
    jumpurl: function (e) {
      var id = e.currentTarget.id;
      // var app = getApp();
      // app.requestDetailid=id;
      console.log(id);
      wx.reLaunch({
        url: id
      })
    },
    returnClick: function () {
      wx.reLaunch({
        url: "/hyb_jianzhi/home/home"
      });
    },
    returnClick: function () {
      wx.reLaunch({
        url: "/hyb_jianzhi/home/home"
      });
    },
    onShareAppMessage: function() {},
  handleChange({ detail }) {
    //console.log(detail);
    this.setData({
      current: detail.key
    });
    if (detail.key == 'homepage') {
      wx.reLaunch({
        url: '../my_release0/my_release'
      })
    }
    if (detail.key == 'group') {
      wx.reLaunch({
        url: '../fabu/fabu'
      })
    }
    if (detail.key == 'remind') {
      wx.reLaunch({
        url: '../message_company/me'
      })
    }
    if (detail.key == 'mine') {
      wx.reLaunch({
        url: '../newstore/newstore'
      })
    }
  }
});