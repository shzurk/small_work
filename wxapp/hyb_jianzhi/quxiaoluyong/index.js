var app = getApp();
const { $Message } = require('../../dist/base/index');
const { $Toast } = require('../../dist/base/index');
Page({
  data: {
    fruit: [{
      id: 1,
      name: '双方友好协商决定取消录取',
    }, {
      id: 2,
      name: '联系不上对方'
    }, {
      id: 3,
      name: '条件不符'
    }, {
      id: 4,
      name: '放鸽子（将限制学生报名，谨慎操作）',
    }],
    current: '',
    position: 'left',
    animal: '熊猫',
    checked: false,
    disabled: false,
    b_no_message:'',
    b_id:'',
    f_id:'',
    f_name:'',
  },
  onLoad:function(e){
    this.setData({
      b_id: e.b_id,
      f_id: e.f_id,
      f_name: e.f_name
    })
  },
  bindmessage:function(e){
    this.setData({
      b_no_message: e.detail.value
    })
  },
  handleFruitChange({ detail = {} }) {
    this.setData({
      current: detail.value
    });
  },
  handleDisabled() {
    this.setData({
      disabled: !this.data.disabled
    });
  },
  handleAnimalChange({ detail = {} }) {
    this.setData({
      checked: detail.current
    });
  },

  submitNoWork:function(){
    var that=this;
    app.util.request({
      url: "entry/wxapp/IsEmployment",
      data: {
        b_id: that.data.b_id,
        is_employment: 0,
        b_no_type: that.data.current,
        b_no_message: that.data.b_no_message
      },
      success: function (data) {
        if (data.data.message == "success") {
          $Message({
            content: '操作成功',
            type: 'success'
          });
          that.Deletecache();
          setTimeout(() => {
            wx.navigateTo({
              url: "/hyb_jianzhi/apply1/apply1?f_id=" + that.data.f_id + "&f_name=" + that.data.f_name,
            });
          }, 1500);
        }
        else
          $Message({
            content: '操作失败',
            type: 'error'
          });
      }
    });
  },
  returnClick: function () {
    wx.reLaunch({
      url: "/hyb_jianzhi/home/home"
    });
  },
  Deletecache: function () {
    var a = {};
    a.f_id = this.data.f_id;
    a.f_name = this.data.f_name;
    var pages = getCurrentPages();
    if (pages.length > 1) {
      var prePage = pages[pages.length - 2];
      prePage.onLoad(a);
    }
    if (pages.length > 2) {
      var preprePage = pages[pages.length - 3];
      preprePage.onLoad()
    }
  }
 
});
