var app = getApp();
Page({

  data: {
    list:''
  },
  onLoad: function (e) {
    var message_id = e.message_id
    var that = this;

    app.util.request({
      url: "entry/wxapp/GetMessageById",
      data: {
        message_id: message_id
      },
      success: function (a) {
        that.setData({
          list: a.data.data
        })
      },
      error: function () {

      }
    });

      app.util.request({
        url: "entry/wxapp/IsLookMsg",
        data: {
          message_id: message_id
        },
        success: function (e) {
          if (e.data.message == "success"){
          var pages = getCurrentPages();
          if (pages.length > 1) {
            //上一个页面实例对象
            var prePage = pages[pages.length - 2];
            //关键在这里,这里面是触发上个界面
            var no_msg = prePage.data.no_msg;
            prePage.setData({
              no_msg: no_msg - 1 >= 0 ? no_msg - 1 : no_msg
            })
          }
          }
          console.log(e);
        }
      });




  },
  returnClick: function () {
    wx.reLaunch({
      url: "/hyb_jianzhi/home/home"
    });
  },

})