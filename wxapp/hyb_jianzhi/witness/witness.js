const { $Toast } = require('../../dist/base/index');
const { $Message } = require('../../dist/base/index');
var app = getApp();

Page({
  data: {
    u_idcard: '',
    u_bname: '',
    openid: ''
  },
  onLoad: function (e) {
    var that = this;
    app.util.request({
      url: "entry/wxapp/GetUserInfo",
      data: {
        openid: wx.getStorageSync("openid")
      },
      success: function (a) {
        that.setData({
          u_bname: a.data.data.u_bname,
          u_idcard: a.data.data.u_idcard==null?'':a.data.data.u_idcard,
          openid: wx.getStorageSync("openid")
        })
      },
      error: function () {

      }
    });
  },
  bindname:function(e){
    this.setData({
      u_bname: e.detail.detail.value
    })
  },
  bindidcard:function(e){
    this.setData({
      u_idcard: e.detail.detail.value
    })
  },
  submituser:function(e){
    if (this.data.u_bname == "") {
      $Toast({
        content: '姓名不能为空',
        type: 'warning'
      });
      return;
    }
    if (this.data.u_idcard == "") {
      $Toast({
        content: '身份证号不能为空',
        type: 'warning'
      });
      return;
    }
    var that = this;
    app.util.request({
      url: "entry/wxapp/UpdateUserInfo",
      data: {
        u_idcard: that.data.u_idcard,
        u_bname: that.data.u_bname,
        openid:that.data.openid
      },
      success: function (data) {
        $Toast.hide();
        console.log(data.data);
        if (data.data.message == "success") {
          $Message({
            content: '操作成功',
            type: 'success'
          });
        }
        else
          $Message({
            content: '操作失败',
            type: 'error'
          });
      },
      error: function () {

      }
    });
  }

})