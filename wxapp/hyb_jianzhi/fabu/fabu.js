var app = getApp();

Page({
    data: {
        openid: "",
        time: "",
        time1: "",
        date: "",
        date1: "",
        typeArray: [],
        types: "",
        timetype:"",
      timetypearray: [],
        index: 0,
        footer: {
            footdex: 1,
            txtcolor: "#A2A2A2",
            seltxt: "#EC6464",
            background: "#fff",
            list: []
        },
        hide_switch: !0,
        accuracy: "",
        loc_lon: "",
        loc_lat: "",
        zongjia: "",
        gender_arr: [ "男", "女", "不限" ],
        gender_index: null,
        is_submit:false,
        settlearr:['日结','周结','月结'],
        settle:'',
      current:'group'

    },
    bindGender: function(e) {
        this.setData({
            gender_index: e.detail.value
        });
    },
    onLoad: function(e) {
        var t = this;
        t.setData({
            openid: wx.getStorageSync("openid")
        }), 
        app.util.request({
          url: "entry/wxapp/GetTimetype",
          success: function (e) {
            var a=[];
            for(var i=0;i<e.data.data.length;i++){
              a.push(e.data.data[i].timetype_name);
            }
            t.setData({
              timetypearray: a
            })
          }
        })
        ,
        app.util.request({
            url: "entry/wxapp/Shangjiainfo",
            data: {
                openid: wx.getStorageSync("openid")
            },
            success: function(e) {
                0 == e.data.data ? wx.showModal({
                    title: "提示",
                    content: "您还未入驻商家",
                    success: function(e) {
                      e.confirm, wx.reLaunch({
                          url: "/hyb_jianzhi/newstore/newstore"
                        });
                    }
                }) : "待审核" == e.data.data.s_typs ? wx.showModal({
                    title: "提示",
                    content: "商家审核中",
                    success: function(e) {
                        e.confirm, wx.reLaunch({
                            url: "/hyb_jianzhi/my_release0/my_release"
                        });
                    }
                }) : e.data.data.jiezhi <= 0 && wx.showModal({
                    title: "提示",
                    content: "商家入驻到期",
                    success: function(e) {
                        e.confirm, wx.reLaunch({
                            url: "/hyb_jianzhi/my/my"
                        });
                    }
                });
            }
        }), t.getBase(), t.getDaohang(), t.getJianzhi_style();
      app.util.request({
        url: "entry/wxapp/GetNolookMsg",
        data: {
          openid: wx.getStorageSync("openid")
        },
        success: function (a) {
          t.setData({
            no_msg: a.data.data
          });

        },
        error: function () {
        }
      });
    },
    getBase: function() {
        var t = this;
        app.util.request({
            url: "entry/wxapp/Base",
            success: function(e) {
                t.setData({
                    base: e.data.data
                });
            }
        });
    },
    getJianzhi_style: function() {
        var i = this;
        app.util.request({
            url: "entry/wxapp/Jianzhi_style",
            success: function(e) {
                var t = [];
                for (var a in e.data.data) t.push(e.data.data[a].title);
                i.setData({
                    typeArray: t
                });
            }
        });
    },
    getDaohang: function() {
        var a = this;
        app.util.request({
            url: "entry/wxapp/Daohang",
            success: function(e) {
                var t = a.data.footer;
                t.list = e.data.data, a.setData({
                    footer: t
                });
            }
        });
    },
    bindTypeChange: function(e) {
        this.setData({
            types: e.detail.value
        });
    },
    bindSeTypeChange:function(e){
      var data = this.data.settlearr;
      console.log(data);
        this.setData({
          settle:data[e.detail.value]
        })
    },
    bindTimeTypeChange: function (e) {
      this.setData({
        timetype: e.detail.value
      });
    },
    bindDateChange: function(e) {
        console.log("picker发送选择改变，携带值为", e.detail.value), this.setData({
            date: e.detail.value
        });
    },
    bindDate: function(e) {
        this.setData({
            date1: e.detail.value
        });
    },
    bindTimeChange: function(e) {
        this.setData({
            time: e.detail.value
        });
    },
    bindTime: function(e) {
        this.setData({
            time1: e.detail.value
        });
    },
    open_map_chonse: function() {
        var t = this;
        wx.chooseLocation({
            success: function(e) {
                t.setData({
                    loc_lon: e.longitude,
                    loc_lat: e.latitude,
                    accuracy: e.address
                });
            },
            fail:function(e){
              wx.showModal({
                title: '提示',
                content: JSON.stringify(e),
                success: function (res) {
                  if (res.confirm) {
                    
                  }
                }
              })
            }
        });
    },
    subClick: function(e) {
      console.log(a);
        var t = this, a = e.detail.value;
      a.daiyu = a.daiyu + "元/" +t.data.timetypearray[t.data.timetype]

        if (a.g_typs = t.data.typeArray[a.types], console.log(a), "" == a.userName) wx.showToast({
            title: "请输入发布名称",
            image: "/hyb_jianzhi/images/errors.png"
        }); else if ("" == a.daiyu) wx.showToast({
            title: "请输入工作待遇",
            image: "/hyb_jianzhi/images/errors.png"
        }); else if ("" == a.types) wx.showToast({
            title: "请选择工作类型",
            image: "/hyb_jianzhi/images/errors.png"
        }); else if ("" == a.usertype) wx.showToast({
            title: "请输入招聘人数",
            image: "/hyb_jianzhi/images/errors.png"
        }); else if ("" == a.sex) wx.showToast({
            title: "请输入招聘性别",
            image: "/hyb_jianzhi/images/errors.png"
        }); else if ("" == a.userNameReal) wx.showToast({
            title: "请输入联系人",
            image: "/hyb_jianzhi/images/errors.png"
        }); else if ("" == a.userPhone) wx.showToast({
            title: "请输入联系电话",
            image: "/hyb_jianzhi/images/errors.png"
        }); else if ("" == a.userFamily) wx.showToast({
            title: "请输入工作内容",
            image: "/hyb_jianzhi/images/errors.png"
        }); else if ("" == a.time || "" == a.time1) wx.showToast({
            title: "请选择工作时间",
            image: "/hyb_jianzhi/images/errors.png"
        }); else if ("" == a.date || "" == a.date1) wx.showToast({
            title: "请选择工作日期",
            image: "/hyb_jianzhi/images/errors.png"
        }); else if ("" == a.address) wx.showToast({
            title: "请选择工作地点",
            image: "/hyb_jianzhi/images/errors.png"
        }); else if ("" == a.settle) wx.showToast({
          title: "请选择结算类型",
          image: "/hyb_jianzhi/images/errors.png"
        }); else {
            var i = a.zhiding_money;
            t.setData({
              is_submit:true
            })
            console.log(t.data.base.zd_type), 0 == t.data.base.zd_type ? app.util.request({
                url: "entry/wxapp/Fabu",
                data: a,
                success: function(e) {
                    wx.showToast({
                        title: "发布成功!"
                    }), setTimeout(function() {
                      t.setData({
                        is_submit: false
                      })
                        wx.redirectTo({
                            url: "/hyb_jianzhi/my_release0/my_release"
                        });
                    }, 1e3);
                }
            }) : 1 == a.zhiding ? 0 == a.zhiding_money ? app.util.request({
                url: "entry/wxapp/Fabu",
                data: a,
                success: function(e) {
                    wx.showToast({
                        title: "发布成功!"
                    }), setTimeout(function() {
                      t.setData({
                        is_submit: false
                      })
                        wx.redirectTo({
                            url: "/hyb_jianzhi/my_release0/my_release"
                        });
                    }, 1e3);
                }
            }) : app.util.request({
                url: "entry/wxapp/Pay",
                data: {
                    openid: a.openid,
                    zhiding_money: i
                },
                header: {
                    "Content-Type": "application/json"
                },
                success: function(e) {
                    wx.requestPayment({
                        timeStamp: e.data.timeStamp,
                        nonceStr: e.data.nonceStr,
                        package: e.data.package,
                        signType: e.data.signType,
                        paySign: e.data.paySign,
                        success: function(e) {
                            app.util.request({
                                url: "entry/wxapp/Fabu",
                                data: a,
                                success: function(e) {
                                    wx.showToast({
                                        title: "发布成功!"
                                    }), setTimeout(function() {
                                      t.setData({
                                        is_submit: false
                                      })
                                        wx.redirectTo({
                                            url: "/hyb_jianzhi/fabu/fabu"
                                        });
                                    }, 1e3);
                                }
                            });
                        }
                    });
                }
            }) : 0 == a.zhiding && app.util.request({
                url: "entry/wxapp/Fabu",
                data: a,
                success: function(e) {
                    wx.showToast({
                        title: "发布成功!"
                    }), setTimeout(function() {
                      t.setData({
                        is_submit: false
                      })
                        wx.redirectTo({
                            url: "/hyb_jianzhi/fabu/fabu"
                        });
                    }, 1e3);
                }
            });
        }
    },
    phoneBlur: function(e) {
        var t = new RegExp(/^1(3|4|5|7|8)\d{9}$/, "g");
        console.log(e), t.test(e.detail.value) || wx.showToast({
            title: "请输入正确的电话",
            icon: "none"
        });
    },
    count: function(e) {
        var t = this.data.base.zd_money, a = e.detail.value, i = parseInt(a) * parseFloat(t);
        this.setData({
            zongjia: i
        });
    },
    switchChange: function(e) {
        console.log(e.detail.value), 1 == e.detail.value ? this.setData({
            hide_switch: !1
        }) : 0 == e.detail.value && this.setData({
            hide_switch: !0
        });
    },
    jumpurl: function (e) {
      var id = e.currentTarget.id;
      // var app = getApp();
      // app.requestDetailid=id;
      console.log(id);
      wx.reLaunch({
        url: id
      })
    },
    returnClick: function () {
      wx.reLaunch({
        url: "/hyb_jianzhi/home/home"
      });
    },
    onShareAppMessage: function() {},
  lookuser: function () {
      wx.navigateTo({
        url: "/hyb_jianzhi/user_diction/index"
      });
    },
  handleChange({ detail }) {
    //console.log(detail);
    this.setData({
      current: detail.key
    });
    if (detail.key == 'homepage') {
      wx.reLaunch({
        url: '../my_release0/my_release'
      })
    }
    if (detail.key == 'group') {
      wx.reLaunch({
        url: '../fabu/fabu'
      })
    }
    if (detail.key == 'remind') {
      wx.reLaunch({
        url: '../message_company/me'
      })
    }
    if (detail.key == 'mine') {
      wx.reLaunch({
        url: '../newstore/newstore'
      })
    }
  }
});