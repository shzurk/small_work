const { $Toast } = require('../../dist/base/index');
const { $Message } = require('../../dist/base/index');
var app = getApp();
Page({

  data: {
    experience_addr: "",
    experience_prize: "",
    start_time: '',
    end_time: '',
    experience_content: ""
  },

  bindaddr: function (e) {
    this.setData({
      experience_addr: e.detail.detail.value
    })
  },

  bindprize: function (e) {
    this.setData({
      experience_prize: e.detail.detail.value
    })
  },

  bindcontent: function (e) {
    this.setData({
      experience_content: e.detail.detail.value
    })
  },


  bindStartDateChange: function (e) {
    this.setData({
      start_time: e.detail.value
    })
  },

  bindEndDateChange: function (e) {
    this.setData({
      end_time: e.detail.value
    })
  },

  onLoad:function(event){
    var experience_id= event.experience_id;
    var that = this;
    app.util.request({
      url: "entry/wxapp/GetExperienceById",
      data: {
        experience_id: experience_id
      },
      success: function (a) {
        that.setData({
          experience_addr: a.data.data.experience_addr,
          experience_prize: a.data.data.experience_prize,
          start_time: a.data.data.start_time,
          end_time: a.data.data.end_time,
          experience_content: a.data.data.experience_content,
          experience_id: a.data.data.experience_id
        })
      },
      error: function () {

      }
    });
  },

  submit: function () {
    if (this.data.experience_addr == "") {
      $Toast({
        content: '组织名称不能为空',
        type: 'warning'
      });
      return;
    }
    if (this.data.experience_prize == "") {
      $Toast({
        content: '职位或奖励不能为空',
        type: 'warning'
      });
      return;
    }
    if (this.data.experience_content == "") {
      $Toast({
        content: '经历描述不能为空',
        type: 'warning'
      });
      return;
    }
    var that = this;
    $Toast({
      content: '正在操作',
      icon: 'prompt',
      duration: 0,
      mask: false
    });
    app.util.request({
      url: "entry/wxapp/UpdateExperience",
      data: {
        experience_addr: that.data.experience_addr,
        experience_prize: that.data.experience_prize,
        start_time: that.data.start_time,
        end_time: that.data.end_time,
        experience_content: that.data.experience_content,
        experience_id: that.data.experience_id
      },
      success: function (data) {
        $Toast.hide();
        console.log(data.data);
        if (data.data.message == "success") {
          $Message({
            content: '修改成功',
            type: 'success'
          });
          setTimeout(() => {
            wx.navigateTo({
              url: '/hyb_jianzhi/schoolexperiencelist/index',
            });
          }, 2000);
        }
        else
          $Message({
            content: '修改失败',
            type: 'error'
          });
      },
      error: function () {

      }
    });
  },
  returnClick: function () {
    wx.reLaunch({
      url: "/hyb_jianzhi/home/home"
    });
  },
})