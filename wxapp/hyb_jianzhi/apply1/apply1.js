const { $Message } = require('../../dist/base/index');
const { $Toast } = require('../../dist/base/index');
var app = getApp();

Page({
  data: {
    f_id: "",
    // tab切换  

    currentTab: 0,
    selectdata: [],
    totalcheck:false
  },
  onLoad: function (a) {
    var t;
    if (a==null){
      var f_id=wx.getStorageSync("f_id");
      var f_name=wx.getStorageSync("f_name");
      t=f_id;
      this.setData({
        f_id:f_id,
        f_name:f_name
      })
    }else{
      t=a.f_id;
      wx.setStorageSync("f_id", t);
      wx.setStorageSync("f_name", a.f_name);
      this.setData({
        f_id: t,
        f_name: a.f_name
      });
    }

    this.getUserbaominglist(t);

    this.employment(t);
    this.pay(t);
    this.appraise(t);

  },
  getUserbaominglist: function (a) {
    var t = this;
    app.util.request({
      url: "entry/wxapp/ShowReport",
      data: {
        f_id: a,
        b_typs: '已报名'
      },
      success: function (a) {
        var tempdata = a.data.data;
        for (var i = 0; i < tempdata.length; i++) {
          tempdata[i]['checked'] = false;
        }
        t.setData({
          list: tempdata
        });
      }
    });
  },
  employment: function (a) {
    var t = this;
    app.util.request({
      url: "entry/wxapp/ShowReport",
      data: {
        f_id: a,
        b_typs: '已录用'
      },
      success: function (a) {
        t.setData({
          employment_list: a.data.data
        });
      }
    });
  },
  pay: function (a) {
    var t = this;
    app.util.request({
      url: "entry/wxapp/ShowReport",
      data: {
        f_id: a,
        b_typs: '已支付'
      },
      success: function (a) {
        t.setData({
          pay_list: a.data.data
        });
      }
    });
  },
  appraise: function (a) {
    var t = this;
    app.util.request({
      url: "entry/wxapp/ShowReport",
      data: {
        f_id: a,
        b_typs: '已评价'
      },
      success: function (a) {
        t.setData({
          appraise_list: a.data.data
        });
      }
    });
  },
  deClick: function (a) {
    var t = a.currentTarget.dataset.id;
    var user_openid = a.currentTarget.dataset.user_openid;
    var that = this;
    wx.navigateTo({
      url: "/hyb_jianzhi/detail1/detail1?b_id=" + t + "&f_id=" + that.data.f_id + "&f_name=" + that.data.f_name + "&user_openid=" + user_openid
    });
  },
  deClick2: function (a) {
    var t = a.currentTarget.dataset.id;
    var that = this;
    wx.navigateTo({
      url: "/hyb_jianzhi/quxiaoluyong/index?b_id=" + t + "&f_id=" + that.data.f_id + "&f_name=" + that.data.f_name
    });
  },
  comment: function (a) {
    var b_id = a.currentTarget.dataset.b_id;
    var user_openid = a.currentTarget.dataset.user_openid;
    var that = this;
    wx.navigateTo({
      url: "/hyb_jianzhi/pingjia1/comment?b_id=" + b_id + "&f_id=" + that.data.f_id + "&f_name=" + that.data.f_name + "&user_openid=" + user_openid
    });
  },
  swichNav: function (e) {

    console.log(e);

    var that = this;

    if (this.data.currentTab === e.target.dataset.current) {

      return false;

    } else {

      that.setData({

        currentTab: e.target.dataset.current,

      })

    }

  },

  swiperChange: function (e) {


    this.setData({

      currentTab: e.detail.current,

    })

  },
  returnxq: function (e) {
    var user_openid = e.currentTarget.dataset.user_openid;
    var f_id = this.data.f_id;
    wx.navigateTo({
      url: '/hyb_jianzhi/details/index?user_openid=' + user_openid + '&f_id=' + f_id,
    })
  },
  returnClick: function () {
    wx.reLaunch({
      url: "/hyb_jianzhi/home/home"
    });
  },
  onPullDownRefresh: function () {
    this.onLoad();
    wx.stopPullDownRefresh();
  },
  deClick3: function (e) {
    var tell = e.currentTarget.dataset.tell;
    wx.makePhoneCall({
      phoneNumber: tell,
    })
  },
  checkboxChange: function (e) {
    console.log(e.detail.value);
    var temp = e.detail.value;
    this.setData({
      selectdata: temp,
      totalcheck:false
    })
  },
  showcheck:function(e){
    var select = [];
    if(e.detail.value[0]==1)
    {
      var temp=this.data.list;
      
      for(var i=0;i<temp.length;i++){
        temp[i]['checked']=true;
        select[i]=temp[i]['b_id'];
      }

    }else{
      var temp = this.data.list;
      for (var i = 0; i < temp.length; i++) {
        temp[i]['checked'] = false;
      }
    }
    this.setData({
      list:temp,
      selectdata:select
    })
  },

  somelu:function(e){
    var that=this;
    if(this.data.selectdata.length==0){
      $Toast({
        content: '没有选中任何报名',
        type: 'warning'
      });
      return;
    }
    var ids=this.data.selectdata.join("-");
    app.util.request({
      url: "entry/wxapp/IsEmployments",
      data: {
        b_ids: ids
      },
      success: function (data) {
        if (data.data.message == "success") {
          that.getUserbaominglist(that.data.f_id);
          $Message({
            content: '操作成功',
            type: 'success'
          });


          var pages = getCurrentPages();
          if (pages.length > 1) {
            //上一个页面实例对象
            var prePage = pages[pages.length - 2];
            //关键在这里,这里面是触发上个界面
            prePage.onLoad()
          }
        }
        else
          $Message({
            content: '操作失败',
            type: 'error'
          });
      }
    });
  }
});