Page({
    data:{
        address:'点击选择地址'
    },
    selectLocation: function (e) {//自行定义tap事件
        var that = this
        wx.chooseLocation({//微信API--打开地图选择位置。
            success: function (res) {//成功之后，目前只返回这四组参数
                that.setData({
                    'address': res.address,
                })
                console.log(res.name), that.setData({ location: { name: res.name, address: res.address, longitude: res.longitude, latitude: res.latitude } })
            },
            fail: function (error) { console.log(error) },
            complete: function (e) { console.log(e) }
        })
    }
 
})